import Resource from "./resource";

/**
 * CRUD action page layout used for Show, Create end Edit.
 */
export default {
  inject: {
    formState: { default: undefined },
    showState: { default: undefined }
  },

  mixins: [Resource],

  props: {
    /**
     * Optional H1 title of the page shown on the left of top header
     */
    title: String
  },
  computed: {
    currentState() {
      return this.formState ?? this.showState;
    },
    item() {
      return this.currentState?.item;
    },
    formResource() {
      return (
        this.resource ??
        this.currentState?.resource ??
        this.$route?.params?.resource
      );
    },
    formTitle() {
      return this.title ?? this.currentState?.title;
    },
    isShowMode() {
      return !!this.showState;
    }
  },
  methods: {
    raiseAction(action, data) {
      const e = {
        action,
        title: this.formTitle,
        resource: this.formResource,
        item: this.item,
        data
      };

      if (this.hasAction(e)) {
        this.$emit("action", e);
        if (this.currentState) {
          this.currentState.raiseAction(e);
        }
      }
    }
  }
};
