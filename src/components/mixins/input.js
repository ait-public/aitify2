import Source from "./source";
import InputWrapper from "./input-wrapper";
import get from "lodash/get";

/**
 * Main input mixin for all inputs used for resource property edition or creation.
 * Auto update the model of the parent form.
 * Use it to create your own input component.
 */
export default {
  mixins: [Source, InputWrapper],
  inject: {
    formState: { default: undefined }
  },
  props: {
    /**
     * By default, the source will be the final name that will be sent to the API for create/update.
     * This prop allows you to override this default behavior.
     */
    model: String
  },
  data() {
    return {
      acceptValue: true,
      input: null,
      internalErrorMessages: []
    };
  },
  watch: {
    value: {
      handler(val) {
        this.input = val;
      },
      immediate: true
    },
    formState: {
      handler(val) {
        if (val) {
          /**
           * Initialize value & errors
           */
          this.refreshInput_(val.item);
        }
      },
      immediate: true
    },
    "formState.item"(val) {
      /**
       * Only if item change, mainly for form in aside case (users).
       */
      this.refreshInput_(val);
    },
    "formState.model"(val) {
      const value = this.getItem(
        get(val || this.formState.model, this.uniqueSourceId)
      );
      this.input = value;
    },
    "formState.errors"(val) {
      if (val) {
        this.internalErrorMessages = val[this.uniqueFormId] || [];
      }
    }
  },
  computed: {
    uniqueSourceId() {
      return [this.parentSource, this.index, this.source]
        .filter(s => s !== undefined)
        .join(".");
    },
    uniqueFormId() {
      return [this.parentSource, this.index, this.model || this.source]
        .filter(s => s !== undefined)
        .join(".");
    },
    commonProps() {
      return {
        label: this.getLabel,
        value: this.input,
        appendIcon: this.appendIcon,
        prependIcon: this.prependIcon,
        prependInnerIcon: this.prependInnerIcon,
        hint: this.hint,
        persistentHint: this.persistentHint,
        rules: this.rules,
        errorMessages: [...this.errorMessages, ...this.internalErrorMessages],
        hideDetails: this.hideDetails,
        dense: this.dense,
        placeholder: this.placeholder,
        clearable: this.clearable,
        readonly: this.readonly,
        disabled: this.disabled,
        autocomplete: "off",
        counter: this.counter,
        counterValue: this.counterValue
      };
    }
  },
  methods: {
    getItem(value) {
      return value === undefined ? this.value : value;
    },
    change(value) {
      /**
       * Triggered on any user input interaction.
       */
      this.$emit("change", value);
    },
    update(value) {
      /**
       * Update model in the injected form if available.
       */
      if (this.formState) {
        this.formState.update({
          source: this.uniqueFormId,
          value
        });
      }

      /**
       * Set value into input.
       */
      this.input = value;

      /**
       * Triggered if value updated.
       */
      this.$emit("input", value);
    },
    keypress($event) {
      this.$emit("keypress", $event);
    },
    keydown($event) {
      this.$emit("keydown", $event);
    },
    drop($event) {
      this.$emit("drop", $event);
    },
    dragover($event) {
      this.$emit("dragover", $event);
    },
    focus() {
      this.$el?.querySelector("input")?.focus();
    },
    refreshInput_(val) {
      if (this.acceptValue) {
        this.update(
          this.getItem(get(val || this.formState.model, this.uniqueSourceId))
        );
      }
    }
  }
};
