import Reference from "./reference";
import Source from "./source";
import truncate from "lodash/truncate";
import get from "lodash/get";
import { createLookup } from "../../lookup";
/**
 * Show value as simple text, render a simple span. HTML tags will be stripped.
 */
export default {
  mixins: [Source, Reference],
  inject: {
    lookupState: { default: createLookup() }
  },
  props: {
    /**
     * Truncate text
     */
    truncate: Number,
    props: {
      /**
       * Value to be selected, array if multiple.
       * @model
       */
      value: {
        type: [String, Number, Array],
        default: null
      },
      /**
       * Name of resource to search into.
       */
      reference: String
    }
  },
  data: () => {
    return {
      loading: false,
      referenceItem: undefined
    };
  },
  computed: {
    lookup() {
      return this.lookupState;
    },
    getItemText() {
      if (!this.referenceItem) {
        return this.input;
      }

      let text = this.itemText || "_display";

      if (typeof text === "function") {
        return text(this.referenceItem);
      }

      return get(this?.referenceItem?.record, text) || this.referenceItem;
    },
    getText() {
      return this.truncate
        ? truncate(this.getItemText, {
            length: this.truncate
          })
        : this.getItemText;
    },
    getLookupItem() {
      return { lookup: this.lookup, val: this.input };
    }
  },

  watch: {
    getLookupItem: {
      async handler() {
        if (this.lookup) {
          this.loading = true;
          const item =
            this.input && (await this.lookup.load(this.reference, this.input));
          this.referenceItem = item || null;
          this.loading = false;
        }
      },
      immediate: true
    }
  }
};
