import Resource from "./resource";

/**
 * CRUD action page layout used for Show, Create end Edit.
 */
export default {
  mixins: [Resource],
  props: {
    item: {
      type: [Object, Array],
      default: null
    }
  },
  methods: {
    raiseAction(action, data) {
      const e = {
        action,
        resource: this.resource,
        item: this.item,
        data
      };

      if (this.hasAction(e)) {
        this.$emit("action", e);
      }
    }
  }
};
