/**
 * Common props for all inputs.
 */
export default {
  props: {
    /**
     * Specific case of parent resource for array input.
     */
    parentSource: String,
    /**
     * Appends an icon to the component. Must be a valid MDI.
     */
    appendIcon: String,

    prependIcon: String,
    prependInnerIcon: String,
    /**
     * Hint text.
     */
    hint: String,
    persistentHint: Boolean,
    /**
     * Hides hint and validation errors. When set to auto messages will be rendered only if there's a message (hint, error message, counter value etc) to display.
     */
    hideDetails: [Boolean, String],
    /**
     * Reduces the input height.
     */
    dense: Boolean,
    /**
     * Add default required client side rule.
     */
    required: Boolean,
    // Changes the style of the input
    solo: Boolean,
    /**
     * Accepts an array of functions that take an input value as an argument and return either true / false or a string with an error message.
     */
    rules: {
      type: Array,
      default() {
        let rules = [];

        if (this.required) {
          rules.push(
            v => !!v || `Поле '${this.label}' не может быть пустым` // this.$t("va.forms.required_field", { field: this.label })
          );
        }
        return rules;
      }
    },
    /**
     * Override default label behavior.
     * Default is to get the localized VueI18n label from both resource and property source.
     */
    label: String,
    /**
     * Override default source key as translated label.
     */
    labelKey: String,
    /**
     * Placeholder if input support it.
     */
    placeholder: String,
    /**
     * Mark this input as clearable.
     */
    clearable: Boolean,
    /**
     * Specific index of field in case of inside array of inputs, aka VaArrayInput.
     * Use it with `parentSource` prop in order to update the value at a good place in the form model.
     */
    index: Number,
    /**
     * List of custom error validation messages to show as hint
     */
    errorMessages: {
      type: Array,
      default: () => []
    },
    requiredMark: {
      type: String,
      default: " *"
    },
    /**
     * Puts input in readonly state
     */
    readonly: {
      type: Boolean,
      default: false
    },
    /**
     * Disable the input
     */
    disabled: {
      type: Boolean,
      default: false
    },
    /**
     * Creates counter for input length; if no number is specified, it defaults to 25. Does not apply any validation
     */
    counter: [Boolean, String, Number],
    counterValue: Function
  },
  computed: {
    getLabel() {
      return this.required ? this.label + this.requiredMark : this.label;
    }
  }
};
