/**
 * For any resource related components.
 */
export default {
  props: {
    /**
     * Name of the resource to use.
     * Required for good label localization and context action activators.
     * Default behavior is to fetch it from router context.
     */
    resource: String
  },
  methods: {
    hasAction(action) {
      return true;
      /**
       * Check if access with user permissions for this specific action
       */
    }
  }
};
