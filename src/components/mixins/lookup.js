import Reference from "./reference";
import Field from "./field";
import truncate from "lodash/truncate";
import get from "lodash/get";
import { createLookup } from "../../lookup";
/**
 * Show value as simple text, render a simple span. HTML tags will be stripped.
 */
export default {
  mixins: [Field, Reference],
  inject: {
    lookupState: { default: createLookup() }
    // formState: { default: undefined },
    // listState: { default: undefined }
  },
  props: {
    /**
     * Truncate text
     */
    truncate: Number
  },
  data: () => {
    return {
      loading: false,
      referenceItem: undefined
    };
  },
  computed: {
    lookup() {
      return this.lookupState;
    },
    getItemText() {
      if (!this.referenceItem) {
        return this.value;
      }

      let text = this.itemText || "_display";

      if (typeof text === "function") {
        return text(this.referenceItem);
      }
      return get(this?.referenceItem?.record, text) || this.referenceItem;
    },
    getText() {
      return this.truncate
        ? truncate(this.getItemText, {
            length: this.truncate
          })
        : this.getItemText;
    },
    getLookupItem() {
      return { lookup: this.lookup, val: this.value };
    }
  },

  watch: {
    getLookupItem: {
      async handler(newVal) {
        if (this.lookup) {
          this.loading = true;
          const item = await this.lookup.load(this.reference, this.value);
          this.referenceItem = item;
          this.loading = false;
        }
      },
      immediate: true
    }
  }
};
