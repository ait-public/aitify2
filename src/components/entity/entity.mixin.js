import { utils } from "@ait/share2";
import raise from "./raise.mixin";
import api from "./api";

import List from "./$mode/List.vue";
import Item from "./$mode/Item.vue";

export const ID_NULL = "00000000-0000-0000-0000-000000000000";

export const LIST_MODE = "list";
export const DEF_VMODE = LIST_MODE;
export const DEF_VIEW = "plain";

const DEF_SCHEMA = "skeleton.foo";

export default {
  mixins: [raise],
  components: {
    List,
    Item
  },
  props: {
    vmode: { type: String },
    id: { type: String },
    view: { type: String },
    reftype: {
      type: String,
      default: DEF_SCHEMA
    },
    idEmpty: {
      type: String,
      default: ID_NULL
    },
    getList: Function,
    getItem: Function,
    createItem: Function,
    refreshData: {
      type: [String, Object, Date, Number]
    },
    idValue: String,
    bigBoss: Boolean,
    skipShow: Boolean,
    defView: String,
    routePath: String
  },
  data() {
    let getList_ = this.getList;
    let getItem_ = this.getItem;

    if (!getList_ || !getItem_) {
      const fapi = api({ schema: this.reftype });
      getList_ = getList_ ?? fapi.getList;
      getItem_ = getItem_ ?? fapi.getItem;
    }

    return {
      vmode_: this.vmode,
      id_: this.id,
      view_: this.view,
      getList_,
      getItem_,
      refreshData_: this.refreshData,
      lastViewList_: this.defView
    };
  },
  computed: {
    propsChanged_() {
      return `${this.vmode}:${this.id}:${this.view}`;
    },
    dataChanges_() {
      return `${this.vmode_}:${this.id_}:${this.view_}`;
    },
    isItemMode_() {
      return this.vmode_ !== DEF_VMODE;
    },
    isListMode_() {
      return !this.isItemMode_;
    },
    isIdEmpty_() {
      return this.id_ === this.idEmpty;
    },
    isCreate_() {
      return this.view_ === "create";
    }
  },
  watch: {
    propsChanged_: {
      immediate: true,
      handler() {
        this.vmode_ = this.vmode || DEF_VMODE;
        this.id_ = this.id || ID_NULL;
        this.view_ = this.view || this.lastViewList_ || DEF_VIEW;
        if (this.isListMode_) {
          // store last list view
          this.lastViewList_ = this.view_;
        }
      }
    },
    dataChanges_: {
      handler() {
        this.raiseUp("propsChanged", {
          vmode: this.vmode_,
          id: this.id_,
          view: this.view_
        });
        this.syncRoute_();
      }
    },
    refreshData() {
      this.refresh();
    }
  },
  methods: {
    onAction_(e) {
      console.log("_", e);
      switch (e.action) {
        case "back":
          this.back_();
          break;
        case "show":
          this.toShow_();
          break;
        case "create":
          this.toCreate_();
          break;
        case "edit":
          this.toEdit_();
          break;
        case "list":
          this.toList_();
          break;
        case "enter:item":
          this.id_ = e.item?.id;
          this.toShowOrEdit_();
          break;
        case "saved":
          this.onSaved_(e?.result);
          break;
        case "item":
          this.raiseUp(e);
          break;
        case "propsChanged":
          break;
        default:
          this.raiseUp(e);
          break;
      }
    },
    back_() {
      switch (this.view_) {
        case "show":
          this.toList_();
          break;
        case "edit":
          this.toShowOrList_();
          break;
        case "create":
          this.toList_();
          break;
      }
    },
    toList_() {
      this.id_ = ID_NULL;
      this.vmode_ = LIST_MODE;
      this.view_ = this.lastViewList_ ?? DEF_VIEW;
    },
    toEdit_() {
      this.view_ = "edit";
      this.vmode_ = "item";
    },
    toShow_(id = undefined) {
      if (id) this.id_ = id;
      this.view_ = "show";
      this.vmode_ = "item";
    },
    toShowOrList_(id) {
      if (this.skipShow) {
        this.toList_();
      } else {
        this.toShow_(id);
      }
    },
    toShowOrEdit_() {
      if (this.skipShow) {
        this.toEdit_();
      } else {
        this.toShow_();
      }
    },
    toCreate_() {
      this.id_ = ID_NULL;
      this.view_ = "create";
      this.vmode_ = "item";
    },
    onSaved_(result) {
      if (!result) {
        this.toList_();
        return;
      }

      this.$aitify.toast.success("Изменения были успешно сохранены");

      if (this.isCreate_) {
        const id = utils.getId(result, {
          defId: this.idValue,
          entityType: this.reftype
        });

        if (id) {
          this.toShowOrList_(id);
        } else {
          this.toList_();
        }
      } else {
        this.toShowOrList_();
        this.refresh();
      }
    },
    syncRoute_() {
      console.log("_sync");
      if (this.routePath && this.bigBoss) {
        this.$router.push(
          `/${this.$aitify.cfg.SCHEMA}/${this.routePath}/${this.vmode_}/${this.id_}/${this.view_}`
        );
      }
    },
    // public methods
    refresh() {
      this.refreshData_ = {};
    },
    defaultHandler(e) {
      console.log(">>>", e);
      if (e?.action === "propsChanged") return;
      this.onAction_(e);
    }
  }
};
