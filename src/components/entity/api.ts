import * as lookup from "../../lookup";

// import { lookup } from "@ait/aitify2";

const lookuper = lookup.createLookup();

export interface IEntityApi {
  // вернуть список
  getList(p: any): Promise<{ data: any[] }>;
  getItem(p: { id: string }): Promise<any>;
}

export default function api(opts: { schema: string }): IEntityApi {
  const { schema } = opts;

  if (!schema) throw new Error("schema not defined");

  // выборка списка через лукап
  const getList = async (p: any) => {
    const data = await lookuper.search(schema, p);
    return {
      data
    };
  };

  // выборка эл-та лукапа
  const getItem = async ({ id }: { id: string }) => {
    if (!id)
      throw new Error(`getItem(${schema}) received a null "id" argument`);
    const ritem = await lookuper.load(schema, id);
    // clear cache
    lookuper.clearAll();
    const item = ritem?.record;
    if (item) return item;
    throw new Error(`Элемент не найден '${schema}' для '${id}'`);
  };

  return {
    getList,
    getItem
  };
}
