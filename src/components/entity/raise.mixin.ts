export default {
  methods: {
    raiseUp(action: string | Record<string, any>, data?: Record<string, any>) {
      if (!action) return;

      let e: string | Record<string, any> | undefined;

      if (typeof action === "string" || action instanceof String) {
        e = {
          action,
          data: {
            ...data
          }
        };
      } else {
        if ((action as any)?.action?.length) {
          e = action;
        }
      }

      if (e) {
        (this as any).$emit("action", e);
      }
    }
  }
};
