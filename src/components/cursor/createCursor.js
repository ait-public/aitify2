export default function createCursor(raiseAction) {
  let items = [];

  const isAround = false;

  let resortTimer = null;

  const state = {
    index: -1,
    position: null,
    count: 0
  };

  function raise(act = "changed:cursor") {
    raiseAction(act, { ...state });
  }

  function setIndex(index) {
    if (index >= 0 && index < state.count) {
      state.index = index;
      state.position = items[index];
      raise();
    }
    return state.index;
  }

  function setPosition(position) {
    return setIndex(items.indexOf(position));
  }

  function toHome() {
    return state.index ? setIndex(0) : state.index;
  }

  function toEnd() {
    return state.count ? setIndex(state.count - 1) : state.index;
  }

  function toUp() {
    const idx = state.index;
    if (idx < 0) return toHome();
    if (isAround && idx === 0) return toEnd();
    if (idx > 0) return setIndex(idx - 1);
    return state.index;
  }

  function toDown() {
    const isLast = state.count && state.count == state.index + 1;
    return isLast && isAround ? toHome() : setIndex(state.index + 1);
  }

  function sync() {
    items = Array.from(items).sort((a, b) => a - b);
    state.count = items.length;
    if (state.position && setPosition(state.position) >= 0) return;

    if (state.index >= state.count) setIndex(state.count - 1);
    else if (state.index >= 0) setIndex(state.index);
  }

  function resortDebounce() {
    clearTimeout(resortTimer);
    resortTimer = setTimeout(sync, 300);
  }

  function add(position) {
    items.push(parseInt(position, 10));
    resortDebounce();
  }

  function clear() {
    clearTimeout(resortTimer);
    resortTimer = null;
    items = [];
    state.index = -1;
    state.count = 0;
    state.position = null;
  }

  function remove(position) {
    const idx = items.indexOf(parseInt(position, 10));
    if (idx >= 0) items.splice(idx, 1);
    state.count = items.length;
    if (idx >= 0 && state.count === 0) {
      clear();
      raise();
    } else if (state.index >= state.count) {
      setIndex(state.count - 1);
    }
    return state.index;
  }

  function reposition({ newpos, oldpos }) {
    const idx = items.indexOf(oldpos);
    if (idx >= 0) {
      items[idx] = newpos;
      if (oldpos === state.position) {
        state.position = newpos;
      }
      resortDebounce();
    }
  }

  const engine = {
    setIndex,
    setPosition,
    reposition,
    // modify
    add,
    remove,
    clear,
    //navigation
    toHome,
    toEnd,
    toUp,
    toDown,
    toLeft: function(event) {
      // engine.raiseAction("left");
    },
    toRight: function(event) {
      // engine.raiseAction("right");
    },
    onKeydown: function(event) {
      if (!event?.which) return;
      const which = event.which;
      let ishandled = false;
      var isown = true;

      switch (which) {
        case 27:
          // if (this.options.onEsc)
          //   this.options.onEsc(event, this.getData(event));
          break;
        case 37:
          // toLeft(event);
          break;
        case 39:
          // toRight(event);
          break;
        case 38:
          toUp(event);
          ishandled = isown;
          break;
        case 40:
          toDown(event);
          ishandled = isown;
          break;
        case 36:
          toHome(event);
          ishandled = isown;
          break;
        case 35:
          toEnd(event);
          ishandled = isown;
          break;
        case 13:
          raise("enter:cursor");
          ishandled = true; //isown;
          break;
      }

      if (ishandled) {
        if (event.preventDefault) {
          event.stopPropagation();
          event.preventDefault();
        }
      }
    }
  };

  return engine;
}
