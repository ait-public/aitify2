export const ACTION = {
  show: { icon: "mdi-eye", text: "Открыть" },
  back: { icon: "mdi-arrow-left", text: "Вернуться" },
  search: { icon: "mdi-magnify", text: "Найти" },
  list: { icon: "mdi-view-list", text: "Список" },
  edit: { icon: "mdi-pencil", text: "Изменить" },
  clone: { icon: "mdi-content-duplicate", text: "Копировать" },
  export: { icon: "mdi-download", text: "Экспорт" },
  print: { icon: "mdi-printer", text: "Печать" },
  delete: { icon: "mdi-trash-can", text: "Удалить" },
  create: { icon: "mdi-plus", text: "Создать" },
  filter: { icon: "mdi-filter-variant-plus", text: "Фильтр" },
  refresh: { icon: "mdi-refresh", text: "Обновить" },
  close: { icon: "mdi-close", text: "Закрыть" },
  save: { icon: "mdi-content-save", text: "Сохранить" },
  apps: { icon: "mdi-apps", text: "Приложения" },
  tools: { icon: "mdi-cog", text: "Настройка" },
  dashboard: { icon: "mdi-view-dashboard", text: "Главная" },
  home: { icon: "mdi-home", text: "Домой" },
  sort: { icon: "mdi-sort", text: "Сортировка" },
  undo: { icon: "mdi-undo", text: "Отменить" }
};
