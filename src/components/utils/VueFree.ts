import Vue from "vue";

const Observer = new Vue().$data.__ob__.constructor;

function prevent(val: any) {
  if (val) {
    // Set dummy observer on value
    Object.defineProperty(val, "__ob__", {
      value: new Observer({}),
      enumerable: false,
      configurable: true
    });
  }

  return val;
}

// vue global
(Vue as any).VUE_FREE = prevent;

// // window
// global.VUE_FREE = prevent

// default export
export default prevent;
