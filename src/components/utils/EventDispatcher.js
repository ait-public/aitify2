class EventDispatcher {
  constructor() {
    this._listeners = {};
  }
  on(type, listener) {
    if (!this._listeners[type]) {
      this._listeners[type] = [];
    }
    this._listeners[type].push(listener);

    const self = this;
    return {
      type: type,
      listener: listener,
      off: function() {
        self.off(type, listener);
        self.listener = null;
      }
    };
  }
  off(type, listener) {
    if (this._listeners[type]) {
      const index = this._listeners[type].indexOf(listener);
      if (index !== -1) {
        this._listeners[type].splice(index, 1);
      }
    }
  }
  //eventMan.notify('evName', 'param1', 20);
  notify(type, listener) {
    if (typeof arguments[0] !== "string") {
      console.warn(
        "EventDispatcher",
        "First params must be an event type (String)"
      );
    } else {
      const listeners = this._listeners[arguments[0]];
      for (const key in listeners) {
        //This could use .apply(arguments) instead, but there is currently a bug with it.
        listeners[key](
          arguments[0],
          arguments[1],
          arguments[2],
          arguments[3],
          arguments[4],
          arguments[5],
          arguments[6]
        );
      }
    }
  }
}

export default EventDispatcher;
