function isWindow(obj) {
  return obj != null && obj === obj.window;
}
function getWindow(elem) {
  return isWindow(elem) ? elem : elem.nodeType === 9 && elem.defaultView;
}
function offsetXY(elem) {
  let box = { top: 0, left: 0 };
  const doc = elem && elem.ownerDocument;
  const docElem = doc.documentElement;

  if (typeof elem.getBoundingClientRect !== typeof undefined) {
    box = elem.getBoundingClientRect();
  }

  const win = getWindow(doc);

  return {
    top: box.top + win.pageYOffset - docElem.clientTop,
    left: box.left + win.pageXOffset - docElem.clientLeft
  };
}

export function getHeight(el) {
  let elHeight = el.offsetHeight;
  elHeight += parseInt(
    window.getComputedStyle(el).getPropertyValue("margin-top")
  );
  elHeight += parseInt(
    window.getComputedStyle(el).getPropertyValue("margin-bottom")
  );
  return elHeight;
}

// function createObserve(el, callback) {
//   let options = {
//     root: el,
//     rootMargin: "0px",
//     threshold: 1.0,
//   };

//   let observer = new IntersectionObserver(callback, options);

//   return prevent(observer);
// }

export function scrollToElm(elm, container = null, troubleshoot = 40) {
  if (!container) {
    var sh = window.innerHeight;
    var st = window.pageYOffset;

    var br = elm.getBoundingClientRect();
    if (br.bottom > sh) {
      //down
      var t = br.bottom - sh;
      window.scroll(0, st + t + troubleshoot);
    } else if (br.top < troubleshoot) {
      //up
      let t = st - (troubleshoot - br.top) - troubleshoot;
      if (t < Math.max(100, troubleshoot)) t = 0;
      window.scroll(0, t);
    }
  } else {
    const elmOffset = offsetXY(elm);
    const parentOffset = offsetXY(container);

    const posTop = elmOffset.top - parentOffset.top - 1;
    const rowHeight = elm.clientHeight;

    const fullHeight = container.clientHeight - rowHeight;

    if (posTop > fullHeight) {
      // Down
      const offsetY = posTop + getHeight(elm) - container.clientHeight;
      const scrollTop = container.scrollTop;
      container.scrollTo(
        container.scrollLeft,
        scrollTop + offsetY + troubleshoot
      );
    } else if (posTop < rowHeight) {
      // Up
      const scrollTop = container.scrollTop;

      let t = scrollTop + posTop - troubleshoot;
      if (t < Math.max(30, troubleshoot)) t = 0;

      container.scrollTo(container.scrollLeft, t);
    }
  }
}
