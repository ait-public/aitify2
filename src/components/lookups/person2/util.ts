export function docSerNum(item: { series?: String; number?: String }) {
  let res = "";
  let ser = (item?.series || "").replace(/\s/g, "");
  let num = (item?.number || "").replace(/\s/g, "");
  if (ser) res = `серия ${ser}`;
  if (ser && num) res += " ";
  if (num && num !== "-") res += `№${num}`;
  return res;
}
