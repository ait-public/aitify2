export { default as AitPersonItem } from "./person/PersonItem.vue";
export { default as AitRefPersonField } from "./person/RefPersonField.vue";
//export { default as AitRefPerson2Field } from "./person/obsolete/RefPerson2Field.vue";
//export { default as AitRefPersonLtField } from "./person/obsolete/RefPersonLtField.vue";
export { default as AitRefPerson } from "./person/RefPerson.vue";
//export { default as AitRefPerson2 } from "./person/obsolete/RefPerson2.vue";
//export { default as AitRefPersonLt } from "./person/obsolete/RefPersonLt.vue";

export { default as AitPersonDocItem } from "./personDoc/PersonDocItem.vue";
export { default as AitRefPersonDoc } from "./personDoc/RefPersonDoc.vue";
export { default as AitRefPersonDocField } from "./personDoc/RefPersonDocField.vue";

export { default as AitContactItem } from "./contact/ContactItem.vue";
export { default as AitRefContact } from "./contact/RefContact.vue";
export { default as AitRefContactField } from "./contact/RefContactField.vue";
