export function getIcon(icon: String) {
  if (!icon) return null;
  const arr = icon.split("#");
  return arr?.length && arr[0];
}
export function getColor(icon: String) {
  if (!icon) return null;
  const arr = icon.split("#");
  return arr?.length > 1 && arr[1];
}
