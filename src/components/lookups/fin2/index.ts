export { default as AitDelegateItem } from "./delegate/DelegateItem.vue";
export { default as AitRefDelegateField } from "./delegate/RefDelegateField.vue";
export { default as AitRefDelegate } from "./delegate/RefDelegate.vue";

export { default as AitContragentItem } from "./contragent/ContragentItem.vue";
export { default as AitRefContragentField } from "./contragent/RefContragentField.vue";
export { default as AitRefContragent } from "./contragent/RefContragent.vue";
