export { default as AitBankDetailItem } from "./bankdetail/BankDetailItem.vue";
export { default as AitRefBankDetailField } from "./bankdetail/RefBankDetailField.vue";
export { default as AitRefBankDetail } from "./bankdetail/RefBankDetail.vue";

export { default as AitAddressItem } from "./address/AddressItem.vue";
export { default as AitRefAddressField } from "./address/RefAddressField.vue";
export { default as AitRefAddress } from "./address/RefAddress.vue";
