export { default as AitEmpRegItem } from "./empReg/EmpRegItem.vue";
export { default as AitRefEmpReg } from "./empReg/RefEmpReg.vue";
export { default as AitRefEmpRegField } from "./empReg/RefEmpRegField.vue";

export { default as AitEmpAssignItem } from "./empAssign/EmpAssignItem.vue";
export { default as AitRefEmpAssign } from "./empAssign/RefEmpAssign.vue";
export { default as AitRefEmpAssignField } from "./empAssign/RefEmpAssignField.vue";

export { default as AitEmpTbn } from "./empReg/view/EmpTbn.vue";
