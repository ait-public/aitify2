// https://github.com/egoist/vue-mugen-scroll
// https://github.com/egoist/element-in-view/blob/master/src/index.js

// Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
// Она сработает только один раз через N миллисекунд после последнего вызова.
// Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
// первого запуска.
function debounce(func, wait, immediate) {
  let timeout;
  return function() {
    const context = this,
      args = arguments;
    const later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

function inViewport(element, { offset = 0, threshold = 0 } = {}) {
  if (!element?.getBoundingClientRect) return false;

  const {
    top,
    right,
    bottom,
    left,
    width,
    height
  } = element.getBoundingClientRect();

  const intersection = {
    t: bottom,
    r: window.innerWidth - left,
    b: window.innerHeight - top,
    l: right
  };

  const elementThreshold = {
    x: threshold * width,
    y: threshold * height
  };

  return (
    intersection.t >= (offset.top || offset + elementThreshold.y) &&
    intersection.r >= (offset.right || offset + elementThreshold.x) &&
    intersection.b >= (offset.bottom || offset + elementThreshold.y) &&
    intersection.l >= (offset.left || offset + elementThreshold.x)
  );
}

const triggers = ["scroll", "resize"];

const AitInfiniteScroll = {
  inject: {
    listState: { default: undefined }
  },
  props: {
    tag: {
      type: String,
      default: "div"
    },
    handler: {
      type: Function
      // required: true
    },
    shouldHandle: {
      type: [Boolean, String],
      default: true
    },
    threshold: {
      type: Number,
      default: 0
    },
    handleOnMount: {
      type: [Boolean, String],
      default: false
    },
    scrollContainer: {
      type: String
    },
    scrollWindow: {
      type: Boolean
    },
    parentScroll: {
      type: [Boolean, String],
      default: true
    },
    debounceTime: {
      type: Number,
      default: 350
    }
  },
  data() {
    return {
      check: null
    };
  },
  computed: {
    canActive() {
      return (
        (!this.isDestroyed && this.handler && this.shouldHandle) ||
        (this.listState && !this.listState.done)
      );
    },
    isReady() {
      return this.listState?.items && !!this.check;
    }
  },
  watch: {
    isReady() {
      if (this.listState && !this.listState.done && this.check) {
        this.check();
      }
    }
  },
  mounted() {
    this.checkInView();
  },
  methods: {
    nextPage() {
      if (this.listState && !this.isDestroyed && !this.listState.done) {
        if (this.checkListUpdate && this.listState.loading) {
          this.checkListUpdate();
          return;
        }

        this.$nextTick(() => {
          if (this.inView) {
            this.listState.options = {
              ...this.listState.options,
              page: (this.listState.options?.page || 0) + 1
            };
          }
        });
      }
    },

    checkInView() {
      if (!this.handler && !this.listState) return;

      if (this.listState) {
        const checkListUpdate = debounce(() => this.nextPage(), 400);
        this.checkListUpdate = checkListUpdate;
        this.listState.isInfinity = true;
      }

      const execute = () => {
        if (!this.canActive) return;

        const inView = inViewport(this.$el, {
          threshold: this.threshold
        });

        this.inView = inView;

        if (inView) {
          if (this.handler && this.shouldHandle) {
            this.handler();
          } else if (
            this.listState &&
            !this.listState.done &&
            this.checkListUpdate
          ) {
            this.checkListUpdate();
          }
        }
      };

      // checkInView right after this component is mounted
      if (this.handleOnMount) {
        execute();
      }

      if (!this.scrollWindow) {
        if (this.scrollContainer) {
          const parent = this.$parent;
          if (parent) {
            while (!this._scrollContainer) {
              if (parent.$el?.className.includes("infinite-scroll")) {
                this._scrollContainer = parent.$el;
                break;
              }
            }
          }
        } else if (this.parentScroll) {
          this._scrollContainer = this.$el.parentElement;
        }

        // Ensure it's html element (ref could be component)
        if (this._scrollContainer && this._scrollContainer.$el) {
          this._scrollContainer = this._scrollContainer.$el;
        }
      }

      this._scrollContainer = this._scrollContainer || window;

      // Add event listeners
      const delay = Math.max(0, this.debounceTime);
      this.check = debounce(execute, delay);
      triggers.forEach(event =>
        this._scrollContainer.addEventListener(event, this.check)
      );
    }
  },
  render(h) {
    return h(
      this.tag || "div",
      {
        staticClass: "ait-infinite-scroll"
        // ref: 'scroll'
      },
      (this.canActive && this.$slots.default) || null
    );
  },
  beforeDestroy() {
    this.isDestroyed = true;
    this.checkListUpdate = null;
    triggers.forEach(event =>
      this._scrollContainer.removeEventListener(event, this.check)
    );
    this.check = null;
  }
};

export default AitInfiniteScroll;
