import "@mdi/font/css/materialdesignicons.css";
import "vuetify/dist/vuetify.min.css";

import Vue from "vue";
import Vuetify from "vuetify";
import { ru } from "vuetify/src/locale";

Vue.use(Vuetify);

const opts = {
  lang: {
    current: "ru",
    locales: { ru },
    t: undefined
  }
};

export default new Vuetify(opts);
