export default {
  list: {
    itemsPerPageOptions: undefined,
    disableExport: false,
    disableGlobalSearch: false
  },
  dateFormat: "dd.MM.yyyy",
  numberFormat: undefined,
  t: {
    "app.title": "Эверест-2",
    "breadcrumbs.home": "Главная",
    "datatable.search": "Найти",
    "datatable.addfilter": "Фильтр",
    "actions.add": "Добавить",
    "actions.show": "Просмотр",
    "actions.create": "Создать",
    "actions.delete": "Удалить",
    "actions.remove": "Удалить",
    "actions.clone": "Копировать",
    "actions.edit": "Изменить",
    "actions.list": "Список",
    "actions.save": "Сохранить",
    "actions.export": "Экспорт",
    "actions.locale": "Locale",
    "actions.move": "Переместить",
    "actions.close": "Закрыть",
    "forms.none": "forms.none",
    "confirm.yes": "Да",
    "confirm.no": "Нет",
    "confirm.canremove": "Удалить?"
  }
};
