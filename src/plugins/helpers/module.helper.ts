import { IModule } from "../../types";
import { RouteConfig } from "vue-router";
import { ModuleTree } from "vuex";

export function mergeModules(modules: IModule[]) {
  return modules.reduce(
    (acc, c) => {
      if (c.route) acc.routes.push(c.route);
      if (c.store) acc.store[c.name] = c.store;
      return acc;
    },
    {
      routes: [] as RouteConfig[],
      store: {} as ModuleTree<any>
    }
  );
}

export function loadModules(modules: any) {
  const r: IModule[] = [];
  for (const name in modules) {
    const module = (modules as any)[name];
    if (module.name) {
      r.push(module);
    }
  }
  return r;
}
