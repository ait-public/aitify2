import { utils } from "@ait/share2";

export function registerComponents(Vue: any, components: any) {
  if (components) {
    for (const componentName in components) {
      const component = (components as any)[componentName];
      const name = utils.kebab_case(componentName);
      // console.log(name);
      if (name) Vue.component(name, component);
    }
  }
}
