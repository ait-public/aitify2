import { utils } from "@ait/share2";

export function registerDirectives(Vue: any, directives: any) {
  for (const directiveName in directives) {
    const directive = (directives as any)[directiveName];
    const name = utils.kebab_case(directiveName);
    if (name) Vue.directive(name, directive);
  }
}
