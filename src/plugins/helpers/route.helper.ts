import { cfg } from "../../cfg";
import VueRouter, { RouteConfig } from "vue-router";
import { auth2Client } from "../../modules/auth2/api";

export function presetRouter({
  router,
  routes
}: {
  router?: VueRouter;
  routes?: RouteConfig[];
}) {
  router = router ?? new VueRouter({ mode: "hash" });

  if (routes) router.addRoutes(routes);
  // set default start page
  router.addRoutes([{ path: "*", redirect: `/${cfg.SCHEMA.toLowerCase()}` }]);

  router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = !!to?.meta?.noAuth; // ["/auth2"];
    // const authRequired = !publicPages.includes(to.path);
    const loggedIn = auth2Client.getLocalUser();

    if (!publicPages && !loggedIn) {
      return next("/auth2");
    }

    next();
  });

  return router;
}
