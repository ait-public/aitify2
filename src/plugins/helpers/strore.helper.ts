import { ModuleTree, Store } from "vuex";

export function presetStore({
  store,
  stores
}: {
  store?: Store<any>;
  stores?: ModuleTree<any>;
}) {
  store = store ?? new Store<any>({});

  if (stores) {
    for (const k of Object.keys(stores)) {
      if (!store.hasModule(k)) {
        const m = stores[k];
        store.registerModule(k, stores[k]);
      }
    }
  }

  return store;
}
