import VueRouter from "vue-router";
import Vuex, { Store } from "vuex";
import PortalVue from "portal-vue";
import { Plugin } from "vue-fragment";
import { utils } from "@ait/share2";

import vuetify from "./vuetify";
import { IModule } from "../types";

import { presetStore } from "./helpers/strore.helper";
import { registerComponents } from "./helpers/components.helper";
import { presetRouter } from "./helpers/route.helper";
import { loadModules, mergeModules } from "./helpers/module.helper";
import DirectivesPlugin from "../directives/Plugin";

import * as components from "../components";
import * as widgets from "../widgets";

import * as modules from "../modules";
import { cfg } from "../cfg";
import options from "./options";
import filters from "../filter";

let insalled = false;

let $aitify: any = null;

export const aitifyPlugin = {
  install(Vue: any) {
    if (insalled) return;

    insalled = true;

    Vue.use(Vuex);
    Vue.use(VueRouter);
    Vue.use(PortalVue);
    Vue.use(Plugin);
    Vue.use(filters);
    Vue.use(DirectivesPlugin);

    Vue.prototype.$aitify = $aitify = {
      bus: new Vue(),
      cfg: { ...cfg },
      options,
      toast: {
        success(message: string) {
          return $aitify.core.store.dispatch("toast/success", message);
        },
        error(message: string, err?: any) {
          console.error(message, err);
          return $aitify.core.store.dispatch("toast/error", message);
        }
      },
      auth2: {
        logout() {
          return $aitify.core.store.dispatch("auth2/logout");
        }
      },
      message: {
        confirm(message: { title: string; message: string }): Promise<any> {
          return $aitify.core.store.dispatch("message/confirm", message);
        }
      }
    };

    registerComponents(Vue, components);
    registerComponents(Vue, widgets);
  },

  presetApp(options?: {
    store?: Store<any>;
    router?: VueRouter;
    modules?: IModule[];
    options?: any;
  }) {
    const m = loadModules(modules);
    const presetModules = [...m, ...(options?.modules ?? [])];

    const combine = mergeModules(presetModules);
    const store = presetStore({ store: options?.store, stores: combine.store });
    const router = presetRouter({
      router: options?.router,
      routes: combine.routes
    });

    if ($aitify) {
      $aitify.core = {
        store,
        models: presetModules
      };

      if (options) {
        $aitify.options = utils.mergeDeep($aitify.options, options.options);
      }

      presetModules.forEach(m => m.init && m.init($aitify));
    }

    return {
      vuetify,
      store,
      router
    };
  }
};

export default aitifyPlugin;

if (typeof window !== "undefined" && window.Vue) {
  window.Vue.use(aitifyPlugin);
}
