import Vue from "vue";

import VueRouter from "vue-router";
import { Store } from "vuex";
import aitifyContext from "./aitifyContext";
import { gqlClient } from "./clients/gql.client";
import { AitClientError } from "./clients/AitClientError";
import { lcStorage } from "./clients/lc.storage";
import { cfg } from "./cfg";

import * as lookup from "./lookup";

import App from "./modules/App.vue";

import aitifyPlugin from "./plugins/aitify.plugin";
import { IModule } from "./types";
import { registerComponents } from "./plugins/helpers/components.helper";

export type TRunAppOptions = {
  el?: any;
  AppVue?: any;
  store?: Store<any>;
  router?: VueRouter;
  modules?: IModule[];
  components?: any;
  options?: {
    auth2?: {
      signinTitle?: string;
    };
  };
};

Vue.config.productionTip = false;

function runApp(options?: TRunAppOptions) {
  Vue.use(aitifyPlugin);

  const AppVue = options?.AppVue ?? App;
  const el = options?.el ?? "#app";

  const preset = aitifyPlugin.presetApp(options);
  // store context
  Object.assign(aitifyContext, preset);
  // components
  registerComponents(Vue, options?.components);

  // main!!
  // new Vue({
  //   ...preset,
  //   render: (h: any) => h(AppVue)
  // }).$mount(el);

  const vm = new Vue({
    data: () => ({ isLoaded: document.readyState === "complete" }),
    ...preset,
    render(h: any) {
      return this.isLoaded ? h(AppVue) : undefined;
    }
  }).$mount(el);

  // Prevent layout jump while waiting for styles
  vm.isLoaded ||
    window.addEventListener("load", () => {
      vm.isLoaded = true;
    });
}

export {
  runApp,
  aitifyContext,
  lcStorage,
  gqlClient,
  lookup,
  cfg,
  AitClientError
};
