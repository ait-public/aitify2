export interface ICfg {
  MEDPORTAL_URL: string;
  GQL_URI: string;
  SCHEMA: string;
  TITLE: string;
  NODE_ENV: string;
  VERSION: string;
  getUrl(schema?: string): string;
}

export function getGqlUrl(schema?: string): string {
  return `${process.env.VUE_BASE_URI}/${schema ||
    process.env.VUE_SCHEMA}/graphql`;
}

const MEDPORTAL_URL = "https://medportal-demo.ru";

const GQL_URI = getGqlUrl();
const SCHEMA = process?.env?.VUE_SCHEMA || "";
const TITLE = process?.env?.VUE_TITLE || "";
const NODE_ENV = process?.env?.NODE_ENV || "";
const VERSION = process?.env?.VUE_APP_VERSION || "";

function getUrl(schema?: string) {
  const schema_ = schema ?? process.env.VUE_SCHEMA;
  if (cfg.NODE_ENV === "production" || schema_ === process.env.VUE_SCHEMA) {
    return `${process.env.VUE_BASE_URI}/${schema_}`;
  }
  return MEDPORTAL_URL + "/" + schema_;
}

export const cfg: ICfg = {
  MEDPORTAL_URL,
  GQL_URI,
  SCHEMA,
  TITLE,
  NODE_ENV,
  VERSION,
  getUrl
};

// App config
