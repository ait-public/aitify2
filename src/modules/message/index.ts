import { IModule } from "../../types";
import store from "./store";

export const message = {
  name: "message",
  store
} as IModule;
