import { RouteConfig } from "vue-router";
import Aitify from "./Aitify.vue";
import Bar from "./demo/entity/Bar.vue";
import Foo from "./demo/entity/Foo.vue";
import Main from "./demo/Main.vue";

const route: RouteConfig = {
  path: "/aitify2",
  component: Aitify,
  children: [
    {
      path: "foo/:vmode?/:id?/:view?",
      component: Foo,
      name: "skeleton--foo",
      props: true
    },
    {
      path: "bar/:vmode?/:id?/:view?",
      component: Bar,
      name: "skeleton--bar",
      props: true
    },
    {
      path: "/"
    }
  ]
};

export default route;
