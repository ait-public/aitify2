import route from "./route";

import { IModule } from "../../types";

export const aitify2 = {
  name: "aitify2",
  route
} as IModule;
