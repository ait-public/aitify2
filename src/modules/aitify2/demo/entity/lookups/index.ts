export { default as AitFooItem } from "./foo/FooItem.vue";
export { default as AitRefFoo } from "./foo/RefFoo.vue";
export { default as AitRefFooField } from "./foo/RefFooField.vue";

export { default as AitBarItem } from "./bar/BarItem.vue";
export { default as AitRefBar } from "./bar/RefBar.vue";
export { default as AitRefBarField } from "./bar/RefBarField.vue";
