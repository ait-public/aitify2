import { gql } from "graphql-request";
import { gqlClient } from "../../../../../../clients/gql.client";
import { IBar, IMutationBarInsertArgs, IMutationBarUpdateArgs } from "../types";

const api = gqlClient.get("skeleton");

const UPDATE = gql`
  mutation barUpdate($input: BarUpdateInput!) {
    barUpdate(input: $input) {
      id
    }
  }
`;

const INSERT = gql`
  mutation Mutation($input: BarInsertInput!) {
    barInsert(input: $input) {
      id
    }
  }
`;

export async function update(input: IMutationBarUpdateArgs): Promise<IBar> {
  return (await api.request(UPDATE, input)).barUpdate;
}

export async function insert(input: IMutationBarInsertArgs): Promise<IBar> {
  return (await api.request(INSERT, input)).barInsert;
}
