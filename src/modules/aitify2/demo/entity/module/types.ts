export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export interface Scalars {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Дата-время без часового пояса в календарной системе ISO-8601, например 2007-12-03T10:15:30, используется для дней рождений... */
  DateTime: any;
  /** A field whose value conforms to the standard internet email address format as specified in RFC822: https://www.w3.org/Protocols/rfc822/. */
  EmailAddress: any;
  /** A field whose value is a generic Universally Unique Identifier: https://en.wikipedia.org/wiki/Universally_unique_identifier. */
  GUID: any;
  /** The `JSONObject` scalar type represents JSON objects as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSONObject: any;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  Json: any;
  /** A local date string (i.e., with no associated timezone) in `YYYY-MM-DD` format, e.g. `2020-01-01`. */
  LocalDate: any;
  /** Дата-время без часового пояса в календарной системе ISO-8601, например 2007-12-03T10:15:30, используется для дней рождений... */
  LocalDateTime: any;
  /** A local time string (i.e., with no associated timezone) in 24-hr `HH:mm[:ss[.SSS]]` format, e.g. `14:25` or `14:25:06` or `14:25:06.123`. */
  LocalTime: any;
}

/** Контакт */
export interface IBar {
  id?: Maybe<Scalars["ID"]>;
  value?: Maybe<Scalars["String"]>;
  fooId?: Maybe<Scalars["String"]>;
  commTypeId?: Maybe<Scalars["String"]>;
}

export interface IBarInsertInput {
  value: Scalars["String"];
  fooId: Scalars["String"];
  commTypeId: Scalars["String"];
}

export interface IBarUpdateInput {
  id: Scalars["ID"];
  value?: InputMaybe<Scalars["String"]>;
  fooId?: InputMaybe<Scalars["String"]>;
  commTypeId?: InputMaybe<Scalars["String"]>;
}

/** Пользователь */
export interface IFoo {
  id?: Maybe<Scalars["ID"]>;
  name?: Maybe<Scalars["String"]>;
}

export interface IFooInsertInput {
  name: Scalars["String"];
}

export interface IFooUpdateInput {
  id: Scalars["ID"];
  name: Scalars["String"];
}

export interface ILookupBarPayload {
  id: Scalars["ID"];
  record?: Maybe<Scalars["JSONObject"]>;
  error?: Maybe<Scalars["String"]>;
}

export interface ILookupFooPayload {
  id: Scalars["ID"];
  record?: Maybe<Scalars["JSONObject"]>;
  error?: Maybe<Scalars["String"]>;
}

export interface ILookupInput {
  foo?: InputMaybe<Array<Scalars["ID"]>>;
  bar?: InputMaybe<Array<Scalars["ID"]>>;
}

export interface ILookupPaginationInput {
  page?: InputMaybe<Scalars["Int"]>;
  perPage?: InputMaybe<Scalars["Int"]>;
}

export interface ILookupResult {
  foo?: Maybe<Array<ILookupFooPayload>>;
  bar?: Maybe<Array<ILookupBarPayload>>;
}

export interface ILookupSearchInput {
  filter?: InputMaybe<Scalars["JSONObject"]>;
  pagination?: InputMaybe<ILookupPaginationInput>;
  sort?: InputMaybe<Array<ILookupSortInput>>;
}

export interface ILookupSortInput {
  by?: InputMaybe<Scalars["String"]>;
  desc?: InputMaybe<Scalars["Boolean"]>;
}

export interface IMutation {
  login?: Maybe<IToken>;
  register?: Maybe<IToken>;
  refreshToken?: Maybe<IToken>;
  logout?: Maybe<Scalars["Boolean"]>;
  /** CRON Команда для периодического выполнения заданий в определённое время */
  runCronJob?: Maybe<Scalars["JSONObject"]>;
  /** инициализация (регистрация) CRON служб */
  initCronJobs?: Maybe<Scalars["JSONObject"]>;
  /** Импорт данных */
  xqImport: IXqResponse;
  /** Создать пользователя (не в БД) */
  fooCreate?: Maybe<IFoo>;
  /** Добавить пользователя */
  fooInsert: IFoo;
  /** Изменить пользователя */
  fooUpdate: IFoo;
  /** Удалить пользователя */
  fooDelete: Scalars["Boolean"];
  /** Создать контакты (не в БД) */
  barCreate?: Maybe<IBar>;
  /** Добавить контакты */
  barInsert: IBar;
  /** Изменить контакты */
  barUpdate: IBar;
  /** Удалить контакты */
  barDelete: Scalars["Boolean"];
}

export interface IMutationLoginArgs {
  name: Scalars["String"];
  password: Scalars["String"];
  tenantId: Scalars["String"];
  profileId?: InputMaybe<Scalars["String"]>;
}

export interface IMutationRegisterArgs {
  name: Scalars["String"];
  password: Scalars["String"];
  email?: InputMaybe<Scalars["String"]>;
}

export interface IMutationRefreshTokenArgs {
  refreshToken: Scalars["String"];
}

export interface IMutationRunCronJobArgs {
  id: Scalars["String"];
  data?: InputMaybe<Scalars["JSONObject"]>;
}

export interface IMutationXqImportArgs {
  input: IXqInput;
}

export interface IMutationFooInsertArgs {
  input: IFooInsertInput;
}

export interface IMutationFooUpdateArgs {
  input: IFooUpdateInput;
}

export interface IMutationFooDeleteArgs {
  id: Scalars["ID"];
}

export interface IMutationBarInsertArgs {
  input: IBarInsertInput;
}

export interface IMutationBarUpdateArgs {
  input: IBarUpdateInput;
}

export interface IMutationBarDeleteArgs {
  id: Scalars["ID"];
}

export interface IQuery {
  findLookup: ILookupResult;
  findLookupList?: Maybe<Array<Scalars["JSONObject"]>>;
  getLookupTypes: Scalars["JSONObject"];
  me?: Maybe<Scalars["JSONObject"]>;
  getCronJobs?: Maybe<Scalars["JSONObject"]>;
  /** Наименование типа */
  foo?: Maybe<Scalars["String"]>;
  /** Получить пользователя */
  fooGetById?: Maybe<IFoo>;
  /** Наименование типа */
  bar?: Maybe<Scalars["String"]>;
  /** Получить контакты */
  barGetById?: Maybe<IBar>;
}

export interface IQueryFindLookupArgs {
  ids: ILookupInput;
}

export interface IQueryFindLookupListArgs {
  type: Scalars["String"];
  where?: InputMaybe<ILookupSearchInput>;
}

export interface IQueryFooGetByIdArgs {
  id: Scalars["ID"];
}

export interface IQueryBarGetByIdArgs {
  id: Scalars["ID"];
}

export interface IToken {
  accessToken?: Maybe<Scalars["String"]>;
  refreshToken?: Maybe<Scalars["String"]>;
}

/**
 * Данные для импорта
 *
 *   data:{
 *     __typename: XqFooType
 *     items:[
 *       {
 *         codeSystem: Foo,
 *         record: {id: 1, value: "value"}
 *       },
 *       {
 *         codeSystem: Bar,
 *         record: {id: 2, value: "value"}
 *       },
 *       {
 *         codeSystem: BarFoo,
 *         record: {id: 3, value: "value"}
 *       }
 *     ]
 *   }
 */
export interface IXqInput {
  data: Scalars["JSONObject"];
}

/**
 * Результат импорта
 *
 *   data:{
 *     __typename: XqFooType
 *     items:[
 *       {
 *         prev: { id: 1, value: "value"}
 *         codeSystem: Foo,
 *         record: {id: convert-1, value: "value"}
 *       },
 *       {
 *         error: "Ошибка импорта v1 (simple)"
 *         codeSystem: Bar,
 *         record: {id: 2, value: "value"}
 *       },
 *       {
 *         codeSystem: BarFoo,
 *         prev: {id: 3, value: "value"},
 *         record: {
 *           nullFlavor: {
 *             code: "XQ",
 *             value: "Ошибка импорта v2 (complex)"
 *           }
 *         }
 *       }
 *     ]
 *   }
 */
export interface IXqResponse {
  data?: Maybe<Scalars["JSONObject"]>;
  /** Ошибка импорта */
  error?: Maybe<Scalars["String"]>;
}
