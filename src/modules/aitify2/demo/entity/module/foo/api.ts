import { gql } from "graphql-request";
import { gqlClient } from "../../../../../../clients/gql.client";
import { IFoo, IMutationFooInsertArgs, IMutationFooUpdateArgs } from "../types";

const api = gqlClient.get("skeleton");

const UPDATE = gql`
  mutation FooUpdate($input: FooUpdateInput!) {
    fooUpdate(input: $input) {
      id
    }
  }
`;

const INSERT = gql`
  mutation Mutation($input: FooInsertInput!) {
    fooInsert(input: $input) {
      id
    }
  }
`;

export async function update(input: IMutationFooUpdateArgs): Promise<IFoo> {
  return (await api.request(UPDATE, input)).fooUpdate;
}

export async function insert(input: IMutationFooInsertArgs): Promise<IFoo> {
  return (await api.request(INSERT, input)).fooInsert;
}
