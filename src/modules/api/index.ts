import { IModule } from "../../types";
import store from "./store";

export const api = {
  name: "api",
  store
} as IModule;
