import { Module } from "vuex";

export type TApiState = {
  loading: boolean;
  refresh: boolean;
};

const message: Module<TApiState, any> = {
  namespaced: true,
  state: {
    loading: false,
    refresh: false
  },
  actions: {
    refresh({ commit, dispatch }, resource) {
      commit("setRefresh", true);

      // return dispatch(
      //   `${resource}/refresh`,
      //   {},
      //   {
      //     root: true,
      //   }
      // );
    }
  },
  mutations: {
    setLoading(state, loading) {
      state.loading = loading;

      if (!loading) {
        state.refresh = false;
      }
    },
    setRefresh(state, refresh) {
      state.refresh = refresh;
    }
  }
};

export default message;
