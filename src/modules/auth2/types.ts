import { ID } from "../../types";

export type Token = {
  accessToken: string;
  refreshToken: string;
};

export type Me = {
  id: ID;
  name: string;
  email: string | null;
  role: string;
  personId: string | null;
  deletedAt?: string | null;
  isDisabled?: boolean;
  profile?: {
    id: ID;
    code?: string | null;
    name?: string | null;
    note?: string | null;
    armId?: string | null;
    arm?: {
      id: ID;
      code?: string | null;
      name?: string | null;
      note?: string | null;
      role?: string | null;
      isDisabled?: boolean;
    };
  };
};

export type IdValue = {
  id: string;
  value: any;
};
