import route from "./route";
import store from "./store";

import { IModule } from "../../types";
import { AitClientError } from "../../clients/AitClientError";

AitClientError.onError = e => console.error(e.dump);

function init($aitify: any) {
  AitClientError.onNonAuthorised = e => {
    console.error("onNonAuthorised", e);
    $aitify.core.store.dispatch("auth2/logout");
  };
}

export const auth2 = {
  name: "auth2",
  route,
  store,
  init
} as IModule;
