import { Module } from "vuex";
import { roles } from "@ait/share2";

import { auth2Client, MeData } from "./api";
import { Me } from "./types";
import aitifyContext from "../../aitifyContext";
import { IDictionary } from "../../types";

// https://jasonwatmore.com/post/2018/07/06/vue-vuex-jwt-authentication-tutorial-example
// https://github.com/cornflourblue/vue-vuex-jwt-authentication-example

const user = auth2Client.getLocalUser();

export type TAuth2State = {
  user: Me | null;
  settings: IDictionary<any> | null;
  status: {
    loggedIn: boolean;
    fail?: string;
  };
  loading: boolean | null;
  forget: {
    emailOrLogin?: string;
  } | null;
  tenantId?: string | null;
  profileId?: string | null;
};

const initialState: TAuth2State = (user as Me)
  ? {
      status: { loggedIn: true },
      user,
      settings: null,
      loading: null,
      forget: null,
      tenantId: null,
      profileId: null
    }
  : {
      status: { loggedIn: false },
      user: null,
      settings: null,
      loading: null,
      forget: null,
      tenantId: null,
      profileId: null
    };

const store: Module<TAuth2State, any> = {
  namespaced: true,
  state: initialState,
  getters: {
    arm(state) {
      const profileId = state.profileId;
      if (!profileId || !state.user) return null;

      if (Array.isArray(state.user.profile)) {
        const profiles: any[] = state.user.profile;
        const p = profiles.find(c => c.id === profileId);
        return p.arm || null;
      }
      return null;
    },
    role(state, getters) {
      const arm = getters.arm;
      if (arm) return arm.role || null;
      return (state.user && state.user.role) || null;
    },
    meId(state) {
      return (state.user && state.user.id) || null;
    },
    meRole(_, getters) {
      return roles.findRole(getters.role);
    },
    isSysadmin(_, getters) {
      return roles.isSysadmin(getters.role);
    },
    isAdmin(_, getters) {
      return roles.isAdmin(getters.role);
    },
    isEmployee(_, getters) {
      return roles.isEmployee(getters.role);
    },
    isPatient(_, getters) {
      return roles.isPatient(getters.role);
    },
    isUser(_, getters) {
      return roles.isUser(getters.role);
    },
    isAnonym(_, getters) {
      return roles.isAnonym(getters.role);
    },
    isAdminOrMore(_, getters) {
      return roles.isAdminOrMore(getters.role);
    },
    isEmployeeOrMore(_, getters) {
      return roles.isEmployeeOrMore(getters.role);
    },
    isPatientOrMore(_, getters) {
      return roles.isPatientOrMore(getters.role);
    },
    isUserOrMore(_, getters) {
      return roles.isUserOrMore(getters.role);
    },
    isAdminOrLess(_, getters) {
      return roles.isAdminOrLess(getters.role);
    },
    isEmployeeOrLess(_, getters) {
      return roles.isEmployeeOrLess(getters.role);
    },
    isPatientOrLess(_, getters) {
      return roles.isPatientOrLess(getters.role);
    },
    isUserOrLess(_, getters) {
      return roles.isUserOrLess(getters.role);
    }
  },
  actions: {
    async login({ dispatch, commit }, { login, password }) {
      commit("loginRequest");
      try {
        const tenantId = this.state.auth2.tenantId;
        console.log("tenant", tenantId);
        const medata: MeData = await auth2Client.login(
          login,
          password,
          tenantId
        );
        commit("loginSuccess", medata);
        // redirect to root
        aitifyContext.goHome();
      } catch (error) {
        let err = "Неправильное имя пользователя или пароль";

        if (typeof error === "string") {
          err = error;
        }

        if ((error as any)?.message) {
          err = (error as any).message;
        }

        commit("loginFailure", err);
        dispatch("toast/error", err, { root: true });
      }
    },
    async init({ dispatch, commit }) {
      try {
        const medata = await auth2Client.load();
        commit("loginSuccess", medata);
      } catch (error) {
        const err = (error as any)?.message.split(":")[0] || "Повторите вход";
        commit("loginFailure", err);
        dispatch("toast/error", err, { root: true });
      }
    },
    async logout({ commit }) {
      if (this.state.auth2?.status?.loggedIn) {
        commit("loading", true);
        try {
          await auth2Client.logout();
        } catch (e) {
          console.error("Ошибка выхода", e);
        }
        commit("logout");
      }
      aitifyContext.goLogin();
    },
    async forgetPsw(
      { dispatch, commit },
      data: { emailOrLogin: string; tenantId: string }
    ) {
      try {
        commit("loading", true);
        const r = await auth2Client.forgetPsw(data);
        dispatch(
          "toast/success",
          `Ссылка для смены пароля отправлена на почтовый адрес: ${data.emailOrLogin}`,
          { root: true }
        );
        commit("loading", false);
        return r;
      } catch (e) {
        commit("loading", false);
        dispatch("toast/error", e, { root: true });
      }
    },
    async chooseProfile({ dispatch, commit }, profileId) {
      const userId: string = this.state.auth2.user?.id || "";
      if (userId) {
        try {
          await auth2Client.selectProfile({ userId, profileId });
          commit("setProfileId", profileId);
        } catch (e) {
          dispatch("toast/error", e, { root: true });
        }
      }
    }
  },
  mutations: {
    loginRequest(state) {
      console.log("loginRequest");
      state.status = { loggedIn: false };
      state.user = null;
      state.profileId = null;
      state.loading = true;
    },
    loginSuccess(state, medata: MeData) {
      state.status = { loggedIn: !!medata.me };
      state.user = medata.me;
      state.settings = medata.settings;
      state.tenantId = medata.tenantId;
      state.profileId = medata.profileId;
      state.loading = false;
    },
    setTenantId(state, tenantId) {
      state.tenantId = tenantId;
      state.status = { loggedIn: false };
      state.user = null;
      state.profileId = null;
      state.loading = false;
    },
    setProfileId(state, profileId) {
      state.profileId = profileId;
    },
    loginFailure(state, fail) {
      state.status = { loggedIn: false, fail };
      state.user = null;
      state.profileId = null;
      state.loading = false;
    },
    logout(state) {
      state.status = { loggedIn: false };
      state.user = null;
      state.loading = false;
      state.profileId = null;
    },
    loading(state, loading) {
      state.loading = loading;
    }
  }
};

export default store;
