import { RouteConfig } from "vue-router";
import Auth from "./Auth.vue";

const auth2Route: RouteConfig = {
  path: "/auth2",
  component: Auth,
  children: [
    {
      path: "",
      component: { template: "<ait-login/>" },
      meta: { noAuth: true }
    }
  ],
  meta: {
    noAuth: true
  }
};

export default auth2Route;
