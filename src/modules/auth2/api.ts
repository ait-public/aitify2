import { gql } from "graphql-request";

import { Me, Token } from "./types";
import { lcStorage } from "../../clients/lc.storage";
import { ID, IDictionary } from "../../types";
import { gqlClient } from "../../clients/gql.client";

const authApi = gqlClient.get("monitor2");

export type MeData = {
  me: Me;
  settings: IDictionary<any>;
  tenantId: ID;
  profileId: ID | null;
};

export interface IClientApi {
  login(login: string, password: string, tenantId: string): Promise<MeData>;
  logout(): Promise<any>;
  register(name: ID, password: string): Promise<Token>;
  load(): Promise<MeData>;
  /** user from local storage */
  getLocalUser(): any;
  forgetPsw(data: { emailOrLogin: string; tenantId: string }): Promise<any>;
  selectProfile(data: { userId: string; profileId: string }): Promise<void>;
}

async function login(
  login: string,
  password: string,
  tenantId: string
): Promise<MeData> {
  const r = await authApi.request(gqlLOGIN, {
    name: login,
    password,
    tenantId
  });

  const t: Token = r.login;

  const r1 = await authApi.request(
    gqlREFRESH_TOKEN,
    { refreshToken: t.refreshToken },
    { Authorization: `Bearer ${t.accessToken}` }
  );

  const t2 = r1.refreshToken;

  // set http headers
  gqlClient.setHeader("Authorization", `Bearer ${t2.accessToken}`);
  // added user from local storage
  lcStorage.setItem("auth2/token", t2);

  const data = await load();
  return data;
}

async function logout() {
  try {
    await authApi.request(gqlLOGOUT);
  } finally {
    // reset http headers
    gqlClient.setHeader("Authorization", ``);
    // remove user from local storage to log user out
    lcStorage.removeItem("auth2/token");
    lcStorage.removeItem("auth2/user");
  }
}

async function load() {
  console.log("load...");
  const r = await authApi.request(gqlME);
  const me = r.me as Me;
  const tenantId = r.tenantId || "";
  const arr: { key: string; value: string }[] = r.getPublicSettings || [];
  const settings = arr.reduce(
    (p, c) => ({ ...p, ...{ [c.key]: c.value } }),
    {} as IDictionary<any>
  );

  lcStorage.setItem("auth2/user", me);
  const userId = me?.id;
  const userData = getUserCache(userId);
  let profileId = userData?.profileId || null;

  const profiles = (Array.isArray(me?.profile) && me.profile) || [];

  if (!profileId) {
    if (profiles.length) {
      profileId = profiles[0].id;
      selectProfile({ userId, profileId });
    }
  } else {
    if (!profiles.some(c => c.id === profileId)) {
      profileId = null;
    }
  }

  return { me, settings, tenantId, profileId };
}

function getUserCache(
  userId: string
): { profileId?: string | null } | undefined {
  return (userId && lcStorage.getItem(userId)) || null;
}

function setUserCache(
  userId: string,
  data: { profileId?: string | null } | null
) {
  lcStorage.setItem(userId, data);
}

async function forgetPsw(data: { emailOrLogin: string; tenantId: string }) {
  const r = await authApi.request(gqlFORGET, data);
  return r.forgetPsw;
}

async function register(name: ID, password: string) {
  const r = await authApi.request(gqlREGISTER, { name, password });
  return r.register as Token;
}

async function selectProfile({
  userId,
  profileId
}: {
  userId: string;
  profileId?: string | null;
}): Promise<void> {
  const userData = getUserCache(userId) || {};
  userData.profileId = profileId || null;
  setUserCache(userId, userData);
}

function getLocalUser() {
  return lcStorage.getItem("auth2/user");
}

// https://jasonwatmore.com/post/2018/07/06/vue-vuex-jwt-authentication-tutorial-example

export const auth2Client: IClientApi = {
  login,
  logout,
  load,
  register,
  getLocalUser,
  forgetPsw,
  selectProfile
};

const gqlLOGIN = gql`
  mutation login($name: String!, $password: String!, $tenantId: String!) {
    login(name: $name, password: $password, tenantId: $tenantId) {
      accessToken
      refreshToken
    }
  }
`;

const gqlREFRESH_TOKEN = gql`
  mutation refreshToken($refreshToken: String!) {
    refreshToken(refreshToken: $refreshToken) {
      accessToken
      refreshToken
    }
  }
`;

const gqlREGISTER = gql`
  mutation register($name: String!, $password: String!) {
    register(name: $name, password: $password) {
      accessToken
      refreshToken
    }
  }
`;

const gqlME = gql`
  {
    me
    getPublicSettings {
      key
      value
    }
    tenantId
  }
`;

const gqlLOGOUT = gql`
  mutation {
    logout
  }
`;

const gqlFORGET = gql`
  mutation forgetPsw($emailOrLogin: String!, $tenantId: String!) {
    forgetPsw(emailOrLogin: $emailOrLogin, tenantId: $tenantId)
  }
`;

function init() {
  const t = lcStorage.getItem("auth2/token");
  gqlClient.setHeader("Authorization", t ? `Bearer ${t.accessToken}` : "");
}

init();
