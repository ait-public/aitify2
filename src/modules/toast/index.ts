import { IModule } from "../../types";
import store from "./store";

export const toast = {
  name: "toast",
  store
} as IModule;
