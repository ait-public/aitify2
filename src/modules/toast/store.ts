import { Module } from "vuex";

export type TToastState = {
  toast: {
    type: string;
    message: string;
  } | null;
};

let clearResolvers: any = [];

const toast: Module<TToastState, any> = {
  namespaced: true,
  state: {
    toast: null
  },
  actions: {
    success({ commit }, message) {
      commit("success", message);
      return new Promise(resolve => {
        clearResolvers.push(resolve);
      });
    },
    error({ commit }, message) {
      commit("error", message);
      return new Promise(resolve => {
        clearResolvers.push(resolve);
      });
    },
    clear({ commit }) {
      commit("clear");
      clearResolvers.forEach((r: any) => r());
      clearResolvers = [];
    }
  },
  mutations: {
    success(state, message) {
      state.toast = { type: "success", message };
    },
    error(state, message) {
      state.toast = { type: "danger", message };
    },
    clear(state) {
      state.toast = null;
    }
  }
};

export default toast;
