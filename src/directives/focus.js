export default {
  inserted: function(el, binding, vnode) {
    setTimeout(() => {
      const inp = el.querySelector("input");
      if (inp?.focus) inp.focus();
      else if (el.focus) el.focus();
    }, 0);
  }
};
