import Cleave from "cleave.js";

function backfix({ el, timerId }) {
  clearTimeout(timerId);
  const input = el._input;
  const res = input._vCleave.properties.result;
  // if (input._value !== res) {
  input.value = res;
  const event = new Event("input", { bubbles: true });
  input.dispatchEvent(event);
  // }
}

// https://buefy.org/extensions/cleavejs/
/**
 * We add a new instance of Cleave when the element
 * is bound and destroy it when it's unbound.
 */
export default {
  bind(el, binding) {
    const input = el.querySelector("input");
    const bindval = binding.value || {};
    input._vCleave = new Cleave(input, bindval);
    if (el) {
      el._input = input;
      el._backfix = !!bindval._backfix;
    }
  },
  update: el => {
    if (el._backfix) {
      const p = { el, timerId: null };
      p.timerId = setTimeout(backfix, 100, p);
    }
  },
  unbind(el) {
    const input = el._input;
    el._input = null;
    input._vCleave.destroy();
  }
};
