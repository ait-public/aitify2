import * as directives from "../directives";
import AuthHref, { IAuthPluginOptions } from "./auth-href";
import { utils } from "@ait/share2";

function registerDirectives(Vue: any, directives: any) {
  for (const directiveName in directives) {
    const directive = (directives as any)[directiveName];
    const name = utils.kebab_case(directiveName);
    if (name) Vue.directive(name, directive);
  }
}

export default {
  install(Vue: any, pluginOptions?: { authHref?: IAuthPluginOptions }) {
    registerDirectives(Vue, directives);

    AuthHref.install(Vue, pluginOptions?.authHref);
  }
};
