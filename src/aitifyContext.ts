import VueRouter from "vue-router";
import { Store } from "vuex";

export type AitifyContext = {
  store?: Store<any>;
  router?: VueRouter;
  vuetify?: any;
  goHome(): void;
  goLogin(): void;
};

function goHome() {
  aitifyContext.router?.push("/");
}

function goLogin() {
  if (aitifyContext.router) {
    if (aitifyContext.router.app) {
      if (aitifyContext.router.app.$route.fullPath.startsWith("/auth2")) {
        return;
      }
    }
    aitifyContext.router.push("/auth2");
  }
}

const aitifyContext: AitifyContext = {
  goHome,
  goLogin
};

export default aitifyContext;
