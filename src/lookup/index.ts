import DataLoader from "@ait/dataloader";
import { gqlClient } from "../clients/gql.client";
import { ID, IDictionary } from "../types";

export type IdsLoader = <T>(
  keys: IDictionary<ID[]>
) => Promise<{ id: ID; record: T }[]>;

const loadFromSchema = async function(
  schema: string,
  keys: IDictionary<ID[]>,
  opts?: { loaders: IDictionary<IdsLoader> }
) {
  // custom or  registered loaders
  const loader: IdsLoader | undefined = opts?.[schema] ?? sources[schema];
  // or def loader
  const pdata = loader ? loader(keys) : defGetByIds(schema, keys);
  // load waiting
  const data = await pdata;
  // map by id
  const res = Object.keys(keys).reduce((p, c) => {
    const acc = (p[c] = {});
    keys[c].forEach((id, idx) => (acc[id] = data[c]?.[idx] || null));
    return p;
  }, {});

  return { [schema]: res };
};

async function load(
  keys: ReadonlyArray<ID>,
  opts?: { loaders: IDictionary<IdsLoader> }
): Promise<any> {
  if (!keys?.length) return Promise.resolve(null);
  const { group, items } = groupBySchemas(keys);
  const all = Object.keys(group).map(schema =>
    loadFromSchema(schema, group[schema], opts)
  );
  let r = await Promise.allSettled(all);
  const roks = r.filter(c => c.status === "fulfilled").map((c: any) => c.value);
  // console.log("roks", roks);
  r.filter(c => c.status === "rejected").forEach(c => {
    console.error("load lookup", c);
  });

  const mp =
    (roks.length && roks.reduce((c, p) => Object.assign(p, c), {})) || null;

  const data = items.map(([schema, typename, value]) => {
    if (!mp) return null;
    return mp?.[schema]?.[typename]?.[value] || null;
  });
  return data;
}

function groupBySchemas(keys: readonly string[]) {
  const items = keys.map(c => c.split("."));

  const group = items.reduce((p, [schema, typename, value]) => {
    const types = p[schema] ?? ({} as IDictionary<ID[]>);
    const ids = types[typename] ?? [];
    types[typename] = ids;
    p[schema] = types;
    ids.push(value);
    return p;
  }, {} as IDictionary<IDictionary<ID[]>>);

  return { group, items };
}

/**
 *
 * @param opts
 * @returns
 */
export function createLookup(opts?: { loaders: IDictionary<IdsLoader> }) {
  const loader = new DataLoader<any, any>(keys => load(keys, opts), {
    batchScheduleFn: callback => setTimeout(callback, 100)
  });

  return {
    load(key: string, id: ID): Promise<any> {
      if (!id) return Promise.resolve(null);
      return loader.load(`${key}.${id}`);
    },
    search,
    clearAll() {
      loader.clearAll();
    }
  };
}

const sources: IDictionary<IdsLoader> = {};

// function sleep(ms) {
//   return new Promise(resolve => setTimeout(resolve, ms));
// }

const getRawByIds = (keys: IDictionary<ID[]>) => {
  const pids = Object.getOwnPropertyNames(keys)
    .map(t => {
      return `${t}:[${keys[t].map(cid => `"${cid}"`).join(",")}]`;
    })
    .join("\r\n");

  const rout = Object.getOwnPropertyNames(keys)
    .map(t => `${t} {id record}`)
    .join("\r\n");

  const query = `{
findLookup(ids: {
${pids}
  }) {
${rout}
  }
} `;

  return query;
};

const defGetByIds = async (schema: string, keys: IDictionary<ID[]>) => {
  const r = await gqlClient.get(schema).rawRequest(getRawByIds(keys));
  if (r.status >= 200 && r.status < 300) {
    return r.data.findLookup;
  }
  return {};
};

/**
 * register lookup callback
 * @param schema
 * @param dictFuncs
 */
export function register(schema: string, dictFuncs: IdsLoader) {
  if (!schema) return;
  sources[schema] = dictFuncs;
}

// ------------------------------------------------------------------------
export type SearchLoader = <T>(criteria: Search) => Promise<T[]>;

export type Sort = {
  by: string;
  desc: boolean;
};

export type Pagination = {
  page: number;
  perPage: number;
};

export type Search = {
  filter?: IDictionary<any>;
  pagination?: Pagination;
  sort?: Sort[];
};

const searches: IDictionary<SearchLoader> = {};

/**
 * Register serach callback function
 * @param key by mask "schema.type" or schema
 * @param func function search or dict of func
 * @returns array of search-items
 */
export function registerSearch(
  key: string,
  func: SearchLoader | IDictionary<SearchLoader>
) {
  if (!key) return;
  if (key.indexOf(".") > 0) {
    searches[key] = func as SearchLoader;
  } else {
    Object.keys(func).forEach(t => (searches[`${key}.${t}`] = func[t]));
  }
}

const FIND_LOOKUP_LIST_QUERY = `
query findLookupList(
  $type: String!
  $where: LookupSearchInput
) {
  findLookupList(
    type: $type
    where: $where
  )
}
`;

const defSearchList = async (key: string, where: Search) => {
  const [schema, type] = key.split(".");
  const r = await gqlClient.get(schema).rawRequest(FIND_LOOKUP_LIST_QUERY, {
    type,
    where: {
      pagination: where.pagination,
      sort: where.sort,
      filter: where.filter
    }
  });
  if (r.status >= 200 && r.status < 300) return r.data.findLookupList;
  return Promise.resolve([]);
};

/**
 * Get Filtered List by search criteria
 * @param key by mask "schema.type"
 * @param criteria data for search
 * @returns array of search-items
 */
export function search(key: string, criteria: Search): Promise<any[]> {
  if (!key) return Promise.resolve([]);
  if (searches[key]) return searches[key](criteria);
  return defSearchList(key, criteria);
}
