import { format, formatRelative } from "date-fns";
import { ru } from "date-fns/locale";

function ago(d: Date | string | undefined) {
  if (!d) return null;
  return formatRelative(new Date(d), new Date(), { locale: ru });
}

function trimFileName(fn?: string | null) {
  if (!fn) return null;

  const idx = fn.startsWith("[") ? fn.indexOf(" — ") : -1;
  if (idx > 0) {
    return fn.substring(idx + 3);
  }

  return fn;
}

export default {
  install: function install(vue: any) {
    vue.filter(
      "ait_date",
      (d: any, f: string | undefined) =>
        (d && format(new Date(d), f || "dd.MM.yyyy HH:mm", { locale: ru })) ||
        null
    );
    vue.filter("ait_date_ago", (d: any) => ago(d));
    vue.filter("ait_trim_filename", (s: any) => trimFileName(s));
  }
};
