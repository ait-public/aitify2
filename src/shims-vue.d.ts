declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

// declare module "vue/types/vue" {
//   export interface Vue {
//     $aitify: any;
//   }
// }

// declare module 'vue/types/vue' {
//   // Глобальные свойства можно объявлять
//   // на интерфейсе `VueConstructor`
//   interface VueConstructor {
//     $store: any
//   }
// }

// // ComponentOptions объявляется в types/options.d.ts
// declare module 'vue/types/options' {
//   interface ComponentOptions<V extends Vue> {
//     $store: any,
//   }
// }

// import Vue, { ComponentOptions } from "vue";
// import { Store } from "./index";

// declare module "vue/types/options" {
//   interface ComponentOptions<V extends Vue> {
//     store?: Store<any>;
//   }
// }

// declare module "vue/types/vue" {
//   interface Vue {
//     $store: Store<any>;
//   }
// }
