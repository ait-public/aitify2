import { IAuthPluginOptions } from "./auth-href";
declare const _default: {
    install(Vue: any, pluginOptions?: {
        authHref?: IAuthPluginOptions | undefined;
    } | undefined): void;
};
export default _default;
//# sourceMappingURL=Plugin.d.ts.map