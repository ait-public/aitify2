export interface IAuthPluginOptions {
    token?: () => string;
    headerAuthKey?: string;
    headerAuthValuePrefix?: string;
    errorHandler?: (e: Error) => void;
    aditionalHeaders?: any;
    textMode?: string;
    downloadingText?: string;
    downloadingHtml?: string;
    dotsAnimation?: boolean;
    overrideInnerHtml?: boolean;
    removeDelay?: string | number;
}
/**
 * https://github.com/nachodd/vue-auth-href
 */
declare const AuthHref: {
    install(Vue: any, pluginOptions?: IAuthPluginOptions | undefined): void;
};
export default AuthHref;
//# sourceMappingURL=auth-href.d.ts.map