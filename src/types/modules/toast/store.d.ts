import { Module } from "vuex";
export declare type TToastState = {
    toast: {
        type: string;
        message: string;
    } | null;
};
declare const toast: Module<TToastState, any>;
export default toast;
//# sourceMappingURL=store.d.ts.map