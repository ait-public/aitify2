import { Me, Token } from "./types";
import { ID, IDictionary } from "../../types";
export declare type MeData = {
    me: Me;
    settings: IDictionary<any>;
    tenantId: ID;
    profileId: ID | null;
};
export interface IClientApi {
    login(login: string, password: string, tenantId: string): Promise<MeData>;
    logout(): Promise<any>;
    register(name: ID, password: string): Promise<Token>;
    load(): Promise<MeData>;
    /** user from local storage */
    getLocalUser(): any;
    forgetPsw(data: {
        emailOrLogin: string;
        tenantId: string;
    }): Promise<any>;
    selectProfile(data: {
        userId: string;
        profileId: string;
    }): Promise<void>;
}
export declare const auth2Client: IClientApi;
//# sourceMappingURL=api.d.ts.map