import { Module } from "vuex";
import { Me } from "./types";
import { IDictionary } from "../../types";
export declare type TAuth2State = {
    user: Me | null;
    settings: IDictionary<any> | null;
    status: {
        loggedIn: boolean;
        fail?: string;
    };
    loading: boolean | null;
    forget: {
        emailOrLogin?: string;
    } | null;
    tenantId?: string | null;
    profileId?: string | null;
};
declare const store: Module<TAuth2State, any>;
export default store;
//# sourceMappingURL=store.d.ts.map