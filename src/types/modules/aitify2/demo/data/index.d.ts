export declare const getDATA: (criteria?: {
    pagination?: {
        page?: number | undefined;
        perPage?: number | undefined;
    } | undefined;
    filter?: {
        q?: string | undefined;
    } | undefined;
} | undefined) => Promise<any[]>;
export declare type UserInfo = {
    id: string;
    firstName: string;
    lastName: string;
    middleName?: string;
    email: string;
    phone?: string;
    sexId?: string;
};
export declare const getUSER_LIST: (criteria?: {
    pagination?: {
        page?: number | undefined;
        perPage?: number | undefined;
    } | undefined;
    filter?: {
        q?: string | undefined;
    } | undefined;
} | undefined) => Promise<UserInfo[]>;
export declare const getUSER: (id: string) => UserInfo;
export declare const getUSER_IDS: (ids: string[]) => Promise<{
    id: string;
    record: UserInfo;
}[]>;
//# sourceMappingURL=index.d.ts.map