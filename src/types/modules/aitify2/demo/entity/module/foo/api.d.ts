import { IFoo, IMutationFooInsertArgs, IMutationFooUpdateArgs } from "../types";
export declare function update(input: IMutationFooUpdateArgs): Promise<IFoo>;
export declare function insert(input: IMutationFooInsertArgs): Promise<IFoo>;
//# sourceMappingURL=api.d.ts.map