import { IBar, IMutationBarInsertArgs, IMutationBarUpdateArgs } from "../types";
export declare function update(input: IMutationBarUpdateArgs): Promise<IBar>;
export declare function insert(input: IMutationBarInsertArgs): Promise<IBar>;
//# sourceMappingURL=api.d.ts.map