import { Module } from "vuex";
export declare type TMessageState = {
    confirm: {
        title: string;
        message: string;
    } | null;
    error: string | null;
    resolve: any | null;
    reject: any | null;
};
declare const message: Module<TMessageState, any>;
export default message;
//# sourceMappingURL=store.d.ts.map