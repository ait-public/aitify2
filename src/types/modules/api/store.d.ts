import { Module } from "vuex";
export declare type TApiState = {
    loading: boolean;
    refresh: boolean;
};
declare const message: Module<TApiState, any>;
export default message;
//# sourceMappingURL=store.d.ts.map