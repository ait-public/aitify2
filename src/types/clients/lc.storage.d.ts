export declare const lcStorage: {
    getItem(k: string): any;
    setItem(k: string, v: any): boolean | undefined;
    removeItem(k: string): boolean | undefined;
};
//# sourceMappingURL=lc.storage.d.ts.map