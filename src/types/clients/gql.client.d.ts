import { GraphQLClient } from "graphql-request";
import { Response } from "node-fetch";
export interface IGqlClient {
    get(schema?: string): GraphQLClient;
    setHeader(key: string, value: string): void;
}
export declare class PostError extends Error {
    url: string;
    status: number;
    constructor(url: string, status: number, message: string);
}
export interface IAitGraphQLClient {
    get: (schema?: string | undefined) => GraphQLClient;
    post: (url: string, data?: any, init?: {
        method?: string | undefined;
        headers?: Record<string, string> | undefined;
        contentType?: string | undefined;
        responseText?: boolean | undefined;
        getResult?: ((r: Response, init?: any) => Promise<any>) | undefined;
        body?: any;
    } | undefined) => Promise<any>;
    setHeader: (key: string, value: string) => void;
}
declare const gqlClient: IAitGraphQLClient;
export { gqlClient };
//# sourceMappingURL=gql.client.d.ts.map