import { ClientError } from "graphql-request";
export declare class AitClientError extends ClientError {
    readonly url?: string;
    readonly nonAuthorised: boolean;
    readonly dump: string;
    static onNonAuthorised: (e: AitClientError) => void;
    static onError: (e: AitClientError) => void;
    constructor(error: ClientError, url?: string);
}
export declare function fail(error: ClientError, url?: string): AitClientError;
//# sourceMappingURL=AitClientError.d.ts.map