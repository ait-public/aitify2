import VueRouter from "vue-router";
import { Store } from "vuex";
export declare type AitifyContext = {
    store?: Store<any>;
    router?: VueRouter;
    vuetify?: any;
    goHome(): void;
    goLogin(): void;
};
declare const aitifyContext: AitifyContext;
export default aitifyContext;
//# sourceMappingURL=aitifyContext.d.ts.map