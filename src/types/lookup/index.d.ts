import { ID, IDictionary } from "../types";
export declare type IdsLoader = <T>(keys: IDictionary<ID[]>) => Promise<{
    id: ID;
    record: T;
}[]>;
/**
 *
 * @param opts
 * @returns
 */
export declare function createLookup(opts?: {
    loaders: IDictionary<IdsLoader>;
}): {
    load(key: string, id: ID): Promise<any>;
    search: typeof search;
    clearAll(): void;
};
/**
 * register lookup callback
 * @param schema
 * @param dictFuncs
 */
export declare function register(schema: string, dictFuncs: IdsLoader): void;
export declare type SearchLoader = <T>(criteria: Search) => Promise<T[]>;
export declare type Sort = {
    by: string;
    desc: boolean;
};
export declare type Pagination = {
    page: number;
    perPage: number;
};
export declare type Search = {
    filter?: IDictionary<any>;
    pagination?: Pagination;
    sort?: Sort[];
};
/**
 * Register serach callback function
 * @param key by mask "schema.type" or schema
 * @param func function search or dict of func
 * @returns array of search-items
 */
export declare function registerSearch(key: string, func: SearchLoader | IDictionary<SearchLoader>): void;
/**
 * Get Filtered List by search criteria
 * @param key by mask "schema.type"
 * @param criteria data for search
 * @returns array of search-items
 */
export declare function search(key: string, criteria: Search): Promise<any[]>;
//# sourceMappingURL=index.d.ts.map