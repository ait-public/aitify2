import VueRouter from "vue-router";
import { Store } from "vuex";
import aitifyContext from "./aitifyContext";
import { gqlClient } from "./clients/gql.client";
import { AitClientError } from "./clients/AitClientError";
import { lcStorage } from "./clients/lc.storage";
import { cfg } from "./cfg";
import * as lookup from "./lookup";
import { IModule } from "./types";
export declare type TRunAppOptions = {
    el?: any;
    AppVue?: any;
    store?: Store<any>;
    router?: VueRouter;
    modules?: IModule[];
    components?: any;
    options?: {
        auth2?: {
            signinTitle?: string;
        };
    };
};
declare function runApp(options?: TRunAppOptions): void;
export { runApp, aitifyContext, lcStorage, gqlClient, lookup, cfg, AitClientError };
//# sourceMappingURL=Aitify.d.ts.map