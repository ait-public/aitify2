declare const _default: {
    list: {
        itemsPerPageOptions: undefined;
        disableExport: boolean;
        disableGlobalSearch: boolean;
    };
    dateFormat: string;
    numberFormat: undefined;
    t: {
        "app.title": string;
        "breadcrumbs.home": string;
        "datatable.search": string;
        "datatable.addfilter": string;
        "actions.add": string;
        "actions.show": string;
        "actions.create": string;
        "actions.delete": string;
        "actions.remove": string;
        "actions.clone": string;
        "actions.edit": string;
        "actions.list": string;
        "actions.save": string;
        "actions.export": string;
        "actions.locale": string;
        "actions.move": string;
        "actions.close": string;
        "forms.none": string;
        "confirm.yes": string;
        "confirm.no": string;
        "confirm.canremove": string;
    };
};
export default _default;
//# sourceMappingURL=options.d.ts.map