import VueRouter, { RouteConfig } from "vue-router";
export declare function presetRouter({ router, routes }: {
    router?: VueRouter;
    routes?: RouteConfig[];
}): VueRouter;
//# sourceMappingURL=route.helper.d.ts.map