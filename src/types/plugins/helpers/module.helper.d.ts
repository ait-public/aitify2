import { IModule } from "../../types";
import { RouteConfig } from "vue-router";
import { ModuleTree } from "vuex";
export declare function mergeModules(modules: IModule[]): {
    routes: RouteConfig[];
    store: ModuleTree<any>;
};
export declare function loadModules(modules: any): IModule[];
//# sourceMappingURL=module.helper.d.ts.map