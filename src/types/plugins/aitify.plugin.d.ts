import VueRouter from "vue-router";
import { Store } from "vuex";
import { IModule } from "../types";
export declare const aitifyPlugin: {
    install(Vue: any): void;
    presetApp(options?: {
        store?: Store<any> | undefined;
        router?: VueRouter | undefined;
        modules?: IModule[] | undefined;
        options?: any;
    } | undefined): {
        vuetify: import("vuetify").default;
        store: Store<any>;
        router: VueRouter;
    };
};
export default aitifyPlugin;
//# sourceMappingURL=aitify.plugin.d.ts.map