export interface ICfg {
    MEDPORTAL_URL: string;
    GQL_URI: string;
    SCHEMA: string;
    TITLE: string;
    NODE_ENV: string;
    VERSION: string;
    getUrl(schema?: string): string;
}
export declare function getGqlUrl(schema?: string): string;
export declare const cfg: ICfg;
//# sourceMappingURL=cfg.d.ts.map