declare const _default: {
    methods: {
        raiseUp(action: string | Record<string, any>, data?: Record<string, any> | undefined): void;
    };
};
export default _default;
//# sourceMappingURL=raise.mixin.d.ts.map