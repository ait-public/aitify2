export interface IEntityApi {
    getList(p: any): Promise<{
        data: any[];
    }>;
    getItem(p: {
        id: string;
    }): Promise<any>;
}
export default function api(opts: {
    schema: string;
}): IEntityApi;
//# sourceMappingURL=api.d.ts.map