export declare const ACTION: {
    show: {
        icon: string;
        text: string;
    };
    back: {
        icon: string;
        text: string;
    };
    search: {
        icon: string;
        text: string;
    };
    list: {
        icon: string;
        text: string;
    };
    edit: {
        icon: string;
        text: string;
    };
    clone: {
        icon: string;
        text: string;
    };
    export: {
        icon: string;
        text: string;
    };
    print: {
        icon: string;
        text: string;
    };
    delete: {
        icon: string;
        text: string;
    };
    create: {
        icon: string;
        text: string;
    };
    filter: {
        icon: string;
        text: string;
    };
    refresh: {
        icon: string;
        text: string;
    };
    close: {
        icon: string;
        text: string;
    };
    save: {
        icon: string;
        text: string;
    };
    apps: {
        icon: string;
        text: string;
    };
    tools: {
        icon: string;
        text: string;
    };
    dashboard: {
        icon: string;
        text: string;
    };
    home: {
        icon: string;
        text: string;
    };
    sort: {
        icon: string;
        text: string;
    };
    undo: {
        icon: string;
        text: string;
    };
};
//# sourceMappingURL=constants.d.ts.map