export { default as AitAddressItem } from "./address/AddressItem.vue";
export { default as AitRefAddress } from "./address/RefAddress.vue";
export { default as AitRefAddressField } from "./address/RefAddressField.vue";
export { default as AitBankDetailItem } from "./bankDetail/BankDetailItem.vue";
export { default as AitRefBankDetailField } from "./bankDetail/RefBankDetailField.vue";
export { default as AitRefBankDetail } from "./bankDetail/RefBankDetail.vue";
//# sourceMappingURL=index.d.ts.map