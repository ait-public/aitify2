import { RouteConfig } from "vue-router";
import { Module } from "vuex";

export interface IDictionary<T> {
  [Key: string]: T;
}

export type ID = string;

export interface IModule {
  name: string;
  store?: Module<any, any>;
  route?: RouteConfig;
  init?: ($aitify: any) => void;
}
