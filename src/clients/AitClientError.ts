import { ClientError } from "graphql-request";

export class AitClientError extends ClientError {
  public readonly url?: string;

  public readonly nonAuthorised: boolean = false;

  public readonly dump: string;

  public static onNonAuthorised = (e: AitClientError) => {};
  public static onError = (e: AitClientError) => {};

  constructor(error: ClientError, url?: string) {
    super(error.response, error.request);
    this.name = "AitClientError";
    this.url = url;
    this.message =
      this.response.errors
        ?.map((c: { message: any }) => c.message)
        .join("\r\n") || error.message;

    if (this.message?.startsWith("Not Authorised!")) {
      this.message = "Не авторизован!";
      this.nonAuthorised = true;
    }

    this.dump = `${this.message}
    ----------------
    ${this.url}
    ----------------
    ${this.request.query}

    ----------------
    variables:
      ${JSON.stringify(this.request.variables)},
    errors:
      ${this.response.errors?.map(c => c.message).join("\r\n")}
    `;

    // console.error(
    //   this.message,
    //   "\r\n----------------\r\n" +
    //   this.url +
    //   "\r\n----------------\r\n" +
    //   this.request.query +
    //   "\r\n----------------\r\n",
    //   this.request.variables,
    //   this.response.errors
    // );
  }
}

export function fail(error: ClientError, url?: string) {
  const e = new AitClientError(error, url);
  AitClientError.onError(e);
  if (e.nonAuthorised) AitClientError.onNonAuthorised(e);
  return e;
}
