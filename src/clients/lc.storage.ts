export const lcStorage = {
  getItem(k: string) {
    if (localStorage) {
      try {
        const json = localStorage.getItem(k);
        if (json) return JSON.parse(json);
      } catch (e) {
        console.error("lcStorage.getItem", e);
      }
    }
    return null;
  },
  setItem(k: string, v: any) {
    if (localStorage) {
      try {
        localStorage.setItem(k, JSON.stringify(v));
        return true;
      } catch (e) {
        console.error("lcStorage.setItem", e);
      }
      return false;
    }
  },
  removeItem(k: string) {
    if (localStorage) {
      try {
        localStorage.removeItem(k);
        return true;
      } catch (e) {
        console.error("lcStorage.removeItem", e);
      }
      return false;
    }
  }
};
