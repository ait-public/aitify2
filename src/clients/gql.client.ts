import { GraphQLClient, ClientError } from "graphql-request";
import fetch, { Response } from "node-fetch";

import { cfg, getGqlUrl } from "../cfg";
import { IDictionary } from "../types";
import { fail } from "./AitClientError";

const clients: IDictionary<GraphQLClient> = {};
const headers: Record<string, string> = {};

const SCHEMA = cfg.SCHEMA;

class GraphQLClientEx extends GraphQLClient {
  private _url: string;
  constructor(url: string, options?: any) {
    super(url, options);
    this._url = url;
  }

  public async request(document: any, variables?: any, requestHeaders?: any) {
    try {
      return await super.request(document, variables, requestHeaders);
    } catch (error) {
      if (error instanceof ClientError) {
        throw fail(error, this._url);
      } else {
        // possibly rethrow or do other things here
        console.error(error);
        throw error;
      }
    }
  }
}

function get(schema?: string) {
  const key = schema ?? SCHEMA;
  if (!clients[key])
    clients[key] = new GraphQLClientEx(getGqlUrl(key), { headers });
  return clients[key];
}

function setHeader(key: string, value: string) {
  headers[key] = value;
  for (const k of Object.keys(clients)) {
    clients[k].setHeaders(headers);
  }
}

export interface IGqlClient {
  get(schema?: string): GraphQLClient;
  setHeader(key: string, value: string): void;
}

export class PostError extends Error {
  url: string;
  status: number;

  constructor(url: string, status: number, message: string) {
    super(message);
    this.name = "AitPostError";
    this.url = url;
    this.status = status;
  }
}

async function getResult(r: Response, asText: boolean = false) {
  if (r.status >= 200 && r.status < 300) {
    return asText ? r.text() : r.json();
  }
  const data = await r.text();
  const heads = r.headers || {};
  const msg = `${r.url} status: ${r?.status}
    headers:
${Object.keys(heads)
  .map(k => `   ${k}: ${heads.get(k)}`)
  .join("\r\n")}

    data:
${data}

`;

  throw new PostError(r.url, r.status, msg);
}

async function post(
  url: string,
  data?: any,
  init?: {
    method?: string;
    headers?: Record<string, string>;
    contentType?: string;
    responseText?: boolean;
    getResult?: (r: Response, init?: any) => Promise<any>;
    body?: any;
  }
) {
  let contentType: string | undefined = init?.contentType ?? "application/json";
  const method = init?.method ?? "POST";

  let body = init?.body;

  if (!body) {
    if (typeof data === "string") {
      body = data;
    } else if (contentType) {
      switch (contentType) {
        case "application/json":
          if (method !== "GET") body = JSON.stringify(data);
          break;
        case "multipart/form-data":
          if (typeof data === "object") {
            // DO NOT supply headers with 'Content-Type' if it's using FormData.
            contentType = undefined;
            const formData = new FormData();
            for (const name in data) {
              if (data[name] !== undefined) formData.append(name, data[name]);
            }
            body = formData;
          }
          break;
      }
    }
  }

  let hds: any;
  if (contentType || init?.headers) {
    hds = {
      ...(contentType && { "Content-Type": contentType }),
      ...headers,
      ...init?.headers
    };
  }

  const res = await fetch(url, {
    method,
    headers: hds,
    body
    // agent: url.startsWith('https://') ? httpsAgent : undefined,
  });

  if (init?.getResult) {
    return init.getResult(res, init);
  }

  return getResult(res, !!init?.responseText);
}

export interface IAitGraphQLClient {
  get: (schema?: string | undefined) => GraphQLClient;
  post: (
    url: string,
    data?: any,
    init?:
      | {
          method?: string | undefined;
          headers?: Record<string, string> | undefined;
          contentType?: string | undefined;
          responseText?: boolean | undefined;
          getResult?: ((r: Response, init?: any) => Promise<any>) | undefined;
          body?: any;
        }
      | undefined
  ) => Promise<any>;
  setHeader: (key: string, value: string) => void;
}

const gqlClient: IAitGraphQLClient = {
  get,
  post,
  setHeader
};

export { gqlClient };
