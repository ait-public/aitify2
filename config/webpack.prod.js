const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");


const configuration = require('./configuration')
const paths = require('./paths')

module.exports = merge(common, {
  mode: 'production',
  devtool: false,
  // devtool: "source-map",
  // optimization: {
  //   minimize: false,
  // },

  optimization: {
    moduleIds: 'deterministic',

    // runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        commons: {
          test: configuration.VENDORS,
          name: 'vendor',
          chunks: 'all',
        }
      }
    },

    minimize: true,
    minimizer: [new CssMinimizerPlugin(), new TerserPlugin()],
    // runtimeChunk: {
    //   name: 'runtime',
    // },

    removeAvailableModules: true
  },



  output: {
    path: paths.build,
    // publicPath: './',
    filename: 'js/[name].[contenthash].bundle.js',
  },
  plugins: [
    // Extracts CSS into separate files
    // Note: style-loader is for development, MiniCssExtractPlugin is for production
    // Извлекать CSS в отдельные файлы
    // Обратите внимание, что style-loader предназначен для разработки, а MiniCssExtractPlugin - для продакшна
    new MiniCssExtractPlugin({
      filename: 'styles/[name].[contenthash].css',
      chunkFilename: '[id].css',
    }),
  ],
  module: {
    rules: [
      {
        test: /.s?css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
    ],
  },

  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
})


