const webpack = require('webpack')
const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')

const configuration = require('./configuration')

process.traceDeprecation = true;

module.exports = merge(common, {
  // Set the mode to development or production
  // Установка режима разработки или продакшна
  mode: 'development',

  // // Control how source maps are generated
  // // Управление созданием карт источников
  // devtool: 'inline-source-map',

  devtool: "source-map",

  optimization: {
    minimize: false,
  },

  // Spin up a server for quick development
  // Запуск сервера для разработки
  devServer: {
    historyApiFallback: true,
    contentBase: "./dist",
    open: true,
    openPage: configuration.SCHEMA,
    compress: false, // true,
    hot: true,
    stats: 'minimal',
    port: configuration.PORT,
    // proxy: configuration.DEV_SERVER_API_HOST || undefined, // 'http://localhost:4000', for web API call test
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers":
        "X-Requested-With, content-type, Authorization",
    },
    overlay: {
      warnings: true,
      errors: true
    },
    // https://webpack.js.org/configuration/dev-server/#devserverproxy
    proxy: {
      [`/${configuration.SCHEMA}/graphql`]: {
        target: configuration.DEV_SERVER_API_HOST,
        secure: false,
        changeOrigin: true
      },
      "/*/graphql": {
        target: configuration.MEDPORTAL_URI,
        secure: false,
        changeOrigin: true
      }
    }
  },

  plugins: [
    // Only update what has changed on hot reload
    // Обновлять только при "горячей" перезагрузке
    new webpack.HotModuleReplacementPlugin(),
  ],
})
