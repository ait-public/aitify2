const NODE_ENV = process.env.NODE_ENV || "development";

require('dotenv-flow').config(); //{ path: path.resolve(__dirname, `../.env.${NODE_ENV}`) })

// get the package metadata from `package.json`
const pkg = require('../package.json')

// set the version
const VERSION = pkg.version

process.env.VUE_APP_VERSION = VERSION;

const envs = Object
    .keys(process.env)
    .filter(k => k.startsWith("VUE_") || k.startsWith("AIT_"))
    .reduce((a, k) => { a[k] = process.env[k]; return a }, {})

let ENVS = {};

Object
    .keys(envs)
    .reduce((a, k) => { a[k] = `${JSON.stringify(envs[k])}`; return a }, ENVS)


const remotes = Object
    .keys(envs)
    .filter(k => k.startsWith("VUE_IMPORT_") && envs[k])
    .reduce((a, k) => { a[k.replace("VUE_IMPORT_", "").toLowerCase()] = envs[k].toLowerCase(); return a; }, {});


const REMOTES = Object
    .keys(remotes)
    .reduce((a, k) => {
        if (remotes[k].startsWith("http")) {
            a[k] = `${k}@${remotes[k]}/remoteEntry.js`
        } else {
            a[k] = `${k}@/${k}/remoteEntry.js`
        }
        return a
    }, {})


const CFG = {
    VERSION,
    MEDPORTAL_URI: "https://medportal-demo.ru",
    TITLE: process.env.VUE_TITLE || 'ait',
    SCHEMA: process.env.VUE_SCHEMA ?? "",
    PORT: process.env.VUE_PORT ?? 3600,
    DEV_SERVER_API_HOST: process.env.VUE_DEV_SERVER_API_HOST ?? "http://localhost:3011",
    REMOTES,
    // EXPOSES: {
    //     "./Login": "./src/components/forms/Login",
    // },
    VENDORS: /[\\/]node_modules[\\/](vue|vue-router|vuex|vuetify|graphql|graphql-request|date-fns)[\\/]/,
    ENVS,
    NODE_ENV
}


console.log(`Client: ver ${VERSION}`, "\r\n", CFG)

module.exports = CFG
