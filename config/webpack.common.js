const paths = require('./paths');
const moduleFederation = require('./module.federation');

const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { VueLoaderPlugin } = require("vue-loader")
const { ModuleFederationPlugin } = webpack.container

const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

const configuration = require('./configuration')
const DoneAsyncPlugin = require("./DoneAsyncPlugin");


module.exports = {

  target: "web",

  // context: __dirname, // to automatically find tsconfig.json

  // Where webpack looks to start building the bundle
  // Откуда начинается сборка
  entry: [paths.src + '/main.ts'],

  // Where webpack outputs the assets and bundles
  // Куда помещаются файлы сборки
  output: {
    path: paths.build,
    filename: 'js/[name].[contenthash].bundle.js',
    // filename: '[name].bundle.js',
    // publicPath: './',
    //  path: path.resolve(__dirname, 'dist'),
    publicPath: "auto",
  },

  // Customize the webpack build process
  // Настройки
  plugins: [
    new webpack.DefinePlugin({
      'process.env': configuration.ENVS
    }),

    new ModuleFederationPlugin({
      ...moduleFederation,
      // library: { type: "var", name: "shell" },
      filename: "remoteEntry.js"
    }),

    new VueLoaderPlugin(),

    // Removes/cleans build folders and unused assets when rebuilding
    // Удаление/очистка директории для файлов сборки и неиспользуемых ресурсов при повтроном сборке
    new CleanWebpackPlugin(),

    // Copies files from target to destination folder
    // Копирование статических файлов
    new CopyWebpackPlugin({
      patterns: [
        {
          from: paths.public,
          to: 'assets',
          globOptions: {
            ignore: ['*.DS_Store'],
          },
        },
      ],
    }),

    // Generates an HTML file from a template
    // Создание HTML-файла на основе шаблона
    new HtmlWebpackPlugin({
      title: configuration.TITLE,
      favicon: paths.src + '/images/favicon.png',
      // template file
      // шаблон
      template: paths.public + '/index.html', // paths.src + '/template.html',
      filename: 'index.html', // output file
      chunks: ["main"],
    }),

    new DoneAsyncPlugin({ options: true }),

    new ForkTsCheckerWebpackPlugin({
      typescript: {
        tsconfig: paths.resolve('../tsconfig.json'),
        extensions: {
          vue: {
            enabled: true
          }
        }
      }
      // checkSyntacticErrors: true,
      // tsconfig: paths.resolve('../tsconfig.json'),
    })

  ],

  // Determine how modules within the project are treated
  // Настройка модулей
  module: {
    rules: [
      // JavaScript: Use Babel to transpile JavaScript files
      // JavaScript: использовать Babel для транспиляции
      { test: /\.js$/, exclude: [/node_modules/], use: ['babel-loader'] },

      {
        test: /\.vue$/,
        loader: "vue-loader",
      },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        // exclude: /node_modules\/(?!(@ait)\/).*/,
        // exclude: /node_modules\/(?!(@ait\/aitify2)\/).*/,
        exclude: [/node_modules/, /src\/types_/],
        options: {
          appendTsSuffixTo: [/\.vue$/],
          allowTsInNodeModules: true,
          // disable type checker - we will use it in fork plugin
          transpileOnly: true
        }
      },

      // Styles: Inject CSS into the head with source maps
      // Стили: встроить CSS в head с картами источников
      {
        test: /\.(scss|css)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { sourceMap: true, importLoaders: 1 },
          },
          { loader: 'postcss-loader', options: { sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } },
        ],
      },

      // Images: Copy image files to build folder
      // Изображения: копировать файлы в директорию для файлов сборки
      { test: /\.(?:ico|gif|png|jpg|jpeg)$/i, type: 'asset/resource' },

      // Fonts and SVGs: Inline files
      // Шрифты и SVG
      { test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, type: 'asset/inline' },
      // {
      //   test: /\.(png|jpg|gif|svg)$/,
      //   loader: 'file-loader',
      //   options: {
      //     name: '[name].[ext]?[hash]'
      //   }
      // }
    ],
  },
  resolve: {
    alias: {
      // vue$: "vue/dist/vue.runtime.esm.js",
      // vuetify: 'vuetify/lib',
      vue$: 'vue/dist/vue.esm.js'
      // vue: "@vue/runtime-dom",
    },

    // vue: "@vue/runtime-dom",
    extensions: ["*", '.ts', ".js", ".vue", ".json"],
  },
  stats: {
    children: true,
  },
}
