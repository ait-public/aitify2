const path = require('path')

// Helpers
const resolve = file => path.resolve(__dirname, file)

module.exports = {
  publicPath: 'auto', // ./skeleton', // auto // './'
  // Source files
  // Исходники
  src: resolve('../src'),
  // Static files that get copied to build folder
  // Статические файлы, которые будут скопированы в директорию для файлов сборки
  public: resolve('../public'),
  // Production build files
  // Директория для файлов сборки
  build: resolve('../../server/public'),
  // path
  resolve, // path.resolve(__dirname, file))
}
