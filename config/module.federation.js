const constants = require('./configuration')


module.exports = {
    name: constants.SCHEMA,
    remotes: constants.REMOTES,
    exposes: constants.EXPOSES
}

