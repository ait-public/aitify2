const configuration = require('./configuration')

module.exports = class DoneAsyncPlugin {
  apply(compiler) {
    compiler.hooks.done.tapAsync('AitClient', (params, callback) => {
      console.log(`

******************************************************************************************

      🚀 Client v${configuration.VERSION} ready at http://localhost:${configuration.PORT}/${configuration.SCHEMA}

******************************************************************************************
      `);
      callback();
    });
  }
}
